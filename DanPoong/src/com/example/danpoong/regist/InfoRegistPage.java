package com.example.danpoong.regist;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import com.example.danpoong.R;
import com.example.danpoong.SplashPage;
import com.example.danpoong.login.LoginPage;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class InfoRegistPage extends Activity implements OnItemSelectedListener {

	// ArrayList mGroupList와 세부 리스트로 사용할 ArrayList<ArrayList<String>>
	// mChildList를 선언하고
	// 세부 리스트의 각각의 항목을 저장할 ArrayList<String> mChildListContent를 선언한다
	// ExpandableListView mListView는 activity_main 에 있는 ExpandableListView 를 지정할
	// 변수

	Spinner spin_hotel_location;
	Spinner spin_hotel_locationgu;
	Spinner spin_bus_location_from;
	Spinner spin_bus_location_to;
	Spinner spin_packed_location;
	Spinner spin_packed_locationgu;
	Spinner spin_res_location;
	Spinner spin_res_locationgu;
	//
	Spinner spin_hotel;
	Spinner spin_bus;
	Spinner spin_packed;
	Spinner spin_res;

	int mYear, mMonth, mDay;
	private TextView mDateDisplay_hotel1;
	private TextView mDateDisplay_hotel2;
	private TextView mDateDisplay_bus1;
	private TextView mDateDisplay_bus2;
	private TextView mDateDisplay_packed1;
	private TextView mDateDisplay_packed2;
	private TextView mDateDisplay_res1;
	private TextView mDateDisplay_res2;
	static int DATE_DIALOG_ID = 0;

	CheckBox checkbox_hotel, checkbox_bus, checkbox_packed, checkbox_res;
	LinearLayout layout_hotel, layout_bus, layout_packed, layout_res;

	// /////////////////////////////
	EditText hotel_editText1, hotel_editText2, hotel_editText5;
	TextView hotel_editText3, hotel_editText4;
	EditText bus_editText1, bus_editText2, bus_editText5;
	TextView bus_editText3, bus_editText4;
	EditText lunch_editText1, lunch_editText2, lunch_editText5;
	TextView lunch_editText3, lunch_editText4;
	EditText res_editText1, res_editText2, res_editText5;
	TextView res_editText3, res_editText4;
	private NotificationManager mNM;
	private Notification mNoti;

	//
	private ProgressDialog pDialog;

	private String[] Location = { "서울", "경기", "인천", "부산", "대구", "광주", "대전",
			"울산", "강원", "경남", "경북", "전남", "전북", "충남", "충북", "제주" };
	private String[] HotelCategory = { "펜션", "민박", "콘도", "호텔", "모텔", "기타" };
	private String[] BusCategory = { "대형버스", "중형버스", "미니버스", "기타" };
	private String[] PackedCategory = { "한식", "양식", "중식", "패스트푸드", "기타" };
	private String[] ResCategory = { "한식", "양식", "중식", "고기", "술집", "기타" };
	//
	String[] noti_str = {"쌀통닭 매장에서 입찰하였습니다.",
			"밥이랑고기랑 매장에서 입찰하였습니다.",
			"용전한우마을 매장에서 입찰하였습니다.",
			"불고기브라더스 매장에서 입찰하였습니다.",
			"계룡대반점 매장에서 입찰하였습니다."};
	int notint = 0;
	//
	HttpPost httppost;
	StringBuffer buffer;
	HttpResponse response;
	HttpClient httpclient;
	List<NameValuePair> nameValuePairs;
	Spinner spin;
	Button signup_button;

	private void HotelSpinners() {
		ArrayAdapter<CharSequence> fAdapter;
		fAdapter = ArrayAdapter.createFromResource(this,
				R.array.spinner_location, R.layout.spinner_info_locationgu);
		fAdapter.setDropDownViewResource(R.layout.spinner_info_category);
		spin_hotel_location.setAdapter(fAdapter);
	}

	private void HotelSubSpinners(int itemNum) {
		ArrayAdapter<CharSequence> fAdapter;
		fAdapter = ArrayAdapter.createFromResource(this, itemNum,
				R.layout.spinner_info_locationgu);
		fAdapter.setDropDownViewResource(R.layout.spinner_info_category);
		spin_hotel_locationgu.setAdapter(fAdapter);
	}

	private OnItemSelectedListener HotelspinSelectedlistener = new OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			switch (position) {
			case (0):
				HotelSubSpinners(R.array.sub_spinner_seoul);
				break;
			case (1):
				HotelSubSpinners(R.array.sub_spinner_gyeonggi);
				break;
			case (2):
				HotelSubSpinners(R.array.sub_spinner_incheon);
				break;
			case (3):
				HotelSubSpinners(R.array.sub_spinner_busan);
				break;
			case (4):
				HotelSubSpinners(R.array.sub_spinner_daegu);
				break;
			case (5):
				HotelSubSpinners(R.array.sub_spinner_daejeon);
				break;
			case (6):
				HotelSubSpinners(R.array.sub_spinner_gwangju);
				break;
			case (7):
				HotelSubSpinners(R.array.sub_spinner_ulsan);
				break;
			case (8):
				HotelSubSpinners(R.array.sub_spinner_ganhwon);
				break;
			case (9):
				HotelSubSpinners(R.array.sub_spinner_gyeongnam);
				break;
			case (10):
				HotelSubSpinners(R.array.sub_spinner_gyeongbuk);
				break;
			case (11):
				HotelSubSpinners(R.array.sub_spinner_geonnam);
				break;
			case (12):
				HotelSubSpinners(R.array.sub_spinner_geonbuk);
				break;
			case (13):
				HotelSubSpinners(R.array.sub_spinner_chungnam);
				break;
			case (14):
				HotelSubSpinners(R.array.sub_spinner_chungbuk);
				break;
			case (15):
				HotelSubSpinners(R.array.sub_spinner_jeju);
				break;
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
		}
	};

	private void PackedSpinners() {
		ArrayAdapter<CharSequence> fAdapter;
		fAdapter = ArrayAdapter.createFromResource(this,
				R.array.spinner_location, R.layout.spinner_info_locationgu);
		fAdapter.setDropDownViewResource(R.layout.spinner_info_category);
		spin_packed_location.setAdapter(fAdapter);
	}

	private void PackedSubSpinners(int itemNum) {
		ArrayAdapter<CharSequence> fAdapter;
		fAdapter = ArrayAdapter.createFromResource(this, itemNum,
				R.layout.spinner_info_locationgu);
		fAdapter.setDropDownViewResource(R.layout.spinner_info_category);
		spin_packed_locationgu.setAdapter(fAdapter);
	}

	private OnItemSelectedListener PackedspinSelectedlistener = new OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			switch (position) {
			case (0):
				PackedSubSpinners(R.array.sub_spinner_seoul);
				break;
			case (1):
				PackedSubSpinners(R.array.sub_spinner_gyeonggi);
				break;
			case (2):
				PackedSubSpinners(R.array.sub_spinner_incheon);
				break;
			case (3):
				PackedSubSpinners(R.array.sub_spinner_busan);
				break;
			case (4):
				PackedSubSpinners(R.array.sub_spinner_daegu);
				break;
			case (5):
				PackedSubSpinners(R.array.sub_spinner_daejeon);
				break;
			case (6):
				PackedSubSpinners(R.array.sub_spinner_gwangju);
				break;
			case (7):
				PackedSubSpinners(R.array.sub_spinner_ulsan);
				break;
			case (8):
				PackedSubSpinners(R.array.sub_spinner_ganhwon);
				break;
			case (9):
				PackedSubSpinners(R.array.sub_spinner_gyeongnam);
				break;
			case (10):
				PackedSubSpinners(R.array.sub_spinner_gyeongbuk);
				break;
			case (11):
				PackedSubSpinners(R.array.sub_spinner_geonnam);
				break;
			case (12):
				PackedSubSpinners(R.array.sub_spinner_geonbuk);
				break;
			case (13):
				PackedSubSpinners(R.array.sub_spinner_chungnam);
				break;
			case (14):
				PackedSubSpinners(R.array.sub_spinner_chungbuk);
				break;
			case (15):
				PackedSubSpinners(R.array.sub_spinner_jeju);
				break;
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
		}
	};

	private void ResSpinners() {
		ArrayAdapter<CharSequence> fAdapter;
		fAdapter = ArrayAdapter.createFromResource(this,
				R.array.spinner_location, R.layout.spinner_info_locationgu);
		fAdapter.setDropDownViewResource(R.layout.spinner_info_category);
		spin_res_location.setAdapter(fAdapter);
	}

	private void ResSubSpinners(int itemNum) {
		ArrayAdapter<CharSequence> fAdapter;
		fAdapter = ArrayAdapter.createFromResource(this, itemNum,
				R.layout.spinner_info_locationgu);
		fAdapter.setDropDownViewResource(R.layout.spinner_info_category);
		spin_res_locationgu.setAdapter(fAdapter);
	}

	private OnItemSelectedListener ResspinSelectedlistener = new OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			switch (position) {
			case (0):
				ResSubSpinners(R.array.sub_spinner_seoul);
				break;
			case (1):
				ResSubSpinners(R.array.sub_spinner_gyeonggi);
				break;
			case (2):
				ResSubSpinners(R.array.sub_spinner_incheon);
				break;
			case (3):
				ResSubSpinners(R.array.sub_spinner_busan);
				break;
			case (4):
				ResSubSpinners(R.array.sub_spinner_daegu);
				break;
			case (5):
				ResSubSpinners(R.array.sub_spinner_daejeon);
				break;
			case (6):
				ResSubSpinners(R.array.sub_spinner_gwangju);
				break;
			case (7):
				ResSubSpinners(R.array.sub_spinner_ulsan);
				break;
			case (8):
				ResSubSpinners(R.array.sub_spinner_ganhwon);
				break;
			case (9):
				ResSubSpinners(R.array.sub_spinner_gyeongnam);
				break;
			case (10):
				ResSubSpinners(R.array.sub_spinner_gyeongbuk);
				break;
			case (11):
				ResSubSpinners(R.array.sub_spinner_geonnam);
				break;
			case (12):
				ResSubSpinners(R.array.sub_spinner_geonbuk);
				break;
			case (13):
				ResSubSpinners(R.array.sub_spinner_chungnam);
				break;
			case (14):
				ResSubSpinners(R.array.sub_spinner_chungbuk);
				break;
			case (15):
				ResSubSpinners(R.array.sub_spinner_jeju);
				break;
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info_regist);

		mDateDisplay_hotel1 = (TextView) findViewById(R.id.reg_hotel_editText3);
		mDateDisplay_hotel2 = (TextView) findViewById(R.id.reg_hotel_editText4);
		mDateDisplay_bus1 = (TextView) findViewById(R.id.reg_bus_editText3);
		mDateDisplay_bus2 = (TextView) findViewById(R.id.reg_bus_editText4);
		mDateDisplay_packed1 = (TextView) findViewById(R.id.reg_lunch_editText3);
		mDateDisplay_packed2 = (TextView) findViewById(R.id.reg_lunch_editText4);
		mDateDisplay_res1 = (TextView) findViewById(R.id.reg_res_editText8);
		mDateDisplay_res2 = (TextView) findViewById(R.id.reg_res_editText9);

		final Calendar c = Calendar.getInstance();

		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);

		mDateDisplay_hotel1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				DATE_DIALOG_ID = 1;
				showDialog(DATE_DIALOG_ID);
				updateDisplay_hotel1();
			}
		});
		mDateDisplay_hotel2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				DATE_DIALOG_ID = 2;
				showDialog(DATE_DIALOG_ID);
				updateDisplay_hotel2();
			}
		});
		mDateDisplay_bus1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				DATE_DIALOG_ID = 3;
				showDialog(DATE_DIALOG_ID);
				updateDisplay_bus1();
			}
		});
		mDateDisplay_bus2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				DATE_DIALOG_ID = 4;
				showDialog(DATE_DIALOG_ID);
				updateDisplay_bus2();
			}
		});
		mDateDisplay_packed1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				DATE_DIALOG_ID = 5;
				showDialog(DATE_DIALOG_ID);
				updateDisplay_packed1();
			}
		});
		mDateDisplay_packed2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				DATE_DIALOG_ID = 6;
				showDialog(DATE_DIALOG_ID);
				updateDisplay_packed2();
			}
		});
		mDateDisplay_res1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				DATE_DIALOG_ID = 7;
				showDialog(DATE_DIALOG_ID);
				updateDisplay_res1();
			}
		});
		mDateDisplay_res2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				DATE_DIALOG_ID = 8;
				showDialog(DATE_DIALOG_ID);
				updateDisplay_res2();
			}
		});

		spin_bus_location_from = (Spinner) findViewById(R.id.reg_bus_spinner1);
		spin_bus_location_to = (Spinner) findViewById(R.id.reg_bus_spinner3);
		spin_hotel = (Spinner) findViewById(R.id.hotel_category_spinner);
		spin_bus = (Spinner) findViewById(R.id.bus_category_spinner);
		spin_packed = (Spinner) findViewById(R.id.lunch_category_spinner);
		spin_res = (Spinner) findViewById(R.id.res_category_spinner);

		// 액티비티 스스로를 선택 변경 이벤트에 대한 리스너로 등록
		spin_bus_location_from.setOnItemSelectedListener(this);
		spin_bus_location_to.setOnItemSelectedListener(this);
		spin_hotel.setOnItemSelectedListener(this);
		spin_bus.setOnItemSelectedListener(this);
		spin_packed.setOnItemSelectedListener(this);
		spin_res.setOnItemSelectedListener(this);

		ArrayAdapter<String> arr_location = new ArrayAdapter<String>(this,
				R.layout.spinner_info_locationgu, Location);
		ArrayAdapter<String> arr_hotel = new ArrayAdapter<String>(this,
				R.layout.spinner_info_locationgu, HotelCategory);
		ArrayAdapter<String> arr_bus = new ArrayAdapter<String>(this,
				R.layout.spinner_info_bus, BusCategory);
		ArrayAdapter<String> arr_packed = new ArrayAdapter<String>(this,
				R.layout.spinner_info_locationgu, PackedCategory);
		ArrayAdapter<String> arr_res = new ArrayAdapter<String>(this,
				R.layout.spinner_info_locationgu, ResCategory);

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowHomeEnabled(false);

		arr_location.setDropDownViewResource(R.layout.spinner_info_category);
		arr_hotel.setDropDownViewResource(R.layout.spinner_info_category);
		arr_bus.setDropDownViewResource(R.layout.spinner_info_category);
		arr_packed.setDropDownViewResource(R.layout.spinner_info_category);
		arr_res.setDropDownViewResource(R.layout.spinner_info_category);

		spin_bus_location_from.setAdapter(arr_location);
		spin_bus_location_to.setAdapter(arr_location);
		spin_hotel.setAdapter(arr_hotel);
		spin_bus.setAdapter(arr_bus);
		spin_packed.setAdapter(arr_packed);
		spin_res.setAdapter(arr_res);

		// hotel spinner초기화
		spin_hotel_location = (Spinner) findViewById(R.id.reg_hotel_spinner1);
		spin_hotel_locationgu = (Spinner) findViewById(R.id.reg_hotel_spinner2);
		HotelSpinners();
		HotelSubSpinners(R.array.sub_spinner_seoul);
		spin_hotel_location
				.setOnItemSelectedListener(HotelspinSelectedlistener);

		// lunch packed spinner초기화
		spin_packed_location = (Spinner) findViewById(R.id.reg_lunch_spinner1);
		spin_packed_locationgu = (Spinner) findViewById(R.id.reg_lunch_spinner2);
		PackedSpinners();
		PackedSubSpinners(R.array.sub_spinner_seoul);
		spin_packed_location
				.setOnItemSelectedListener(PackedspinSelectedlistener);

		// res packed spinner초기화
		spin_res_location = (Spinner) findViewById(R.id.reg_res_spinner1);
		spin_res_locationgu = (Spinner) findViewById(R.id.reg_res_spinner2);
		ResSpinners();
		ResSubSpinners(R.array.sub_spinner_seoul);
		spin_res_location.setOnItemSelectedListener(ResspinSelectedlistener);

		checkbox_hotel = (CheckBox) findViewById(R.id.reg_hotel_checkBox);
		checkbox_hotel.setChecked(false);
		checkbox_bus = (CheckBox) findViewById(R.id.reg_bus_checkBox);
		checkbox_bus.setChecked(false);
		checkbox_packed = (CheckBox) findViewById(R.id.reg_lunch_checkBox);
		checkbox_packed.setChecked(false);
		checkbox_res = (CheckBox) findViewById(R.id.reg_res_checkBox);
		checkbox_res.setChecked(false);

		layout_hotel = (LinearLayout) findViewById(R.id.reg_hotel_layout);
		layout_bus = (LinearLayout) findViewById(R.id.reg_bus_layout);
		layout_packed = (LinearLayout) findViewById(R.id.reg_packed_layout);
		layout_res = (LinearLayout) findViewById(R.id.reg_res_layout);

		layout_hotel.setVisibility(View.GONE);
		layout_bus.setVisibility(View.GONE);
		layout_packed.setVisibility(View.GONE);
		layout_res.setVisibility(View.GONE);

		checkbox_hotel.setOnClickListener(new OnClickListener() { // checkbox
					// listener
					public void onClick(View v) {
						// Perform action on clicks, depending on whether it's
						// now checked
						if (((CheckBox) v).isChecked()) {
							layout_hotel.setVisibility(View.VISIBLE);
						} else if (((CheckBox) v).isChecked() == false) {
							layout_hotel.setVisibility(View.GONE);
						}
					}
				});

		checkbox_bus.setOnClickListener(new OnClickListener() { // checkbox
					// listener
					public void onClick(View v) {
						// Perform action on clicks, depending on whether it's
						// now checked
						if (((CheckBox) v).isChecked()) {
							layout_bus.setVisibility(View.VISIBLE);
						} else if (((CheckBox) v).isChecked() == false) {
							layout_bus.setVisibility(View.GONE);
						}
					}
				});

		checkbox_packed.setOnClickListener(new OnClickListener() { // checkbox
					// listener
					public void onClick(View v) {
						// Perform action on clicks, depending on whether it's
						// now checked
						if (((CheckBox) v).isChecked()) {
							layout_packed.setVisibility(View.VISIBLE);
						} else if (((CheckBox) v).isChecked() == false) {
							layout_packed.setVisibility(View.GONE);
						}
					}
				});

		checkbox_res.setOnClickListener(new OnClickListener() { // checkbox
					// listener
					public void onClick(View v) {
						// Perform action on clicks, depending on whether it's
						// now checked
						if (((CheckBox) v).isChecked()) {
							layout_res.setVisibility(View.VISIBLE);
						} else if (((CheckBox) v).isChecked() == false) {
							layout_res.setVisibility(View.GONE);
						}
					}
				});
		// /////////////////////////////////////////////////////////////////////////
		// db
		hotel_editText1 = (EditText) findViewById(R.id.reg_hotel_editText1);
		hotel_editText2 = (EditText) findViewById(R.id.reg_hotel_editText2);
		hotel_editText3 = (TextView) findViewById(R.id.reg_hotel_editText3);
		hotel_editText4 = (TextView) findViewById(R.id.reg_hotel_editText4);
		hotel_editText5 = (EditText) findViewById(R.id.reg_hotel_editText5);
		//
		bus_editText1 = (EditText) findViewById(R.id.reg_bus_editText1);
		bus_editText2 = (EditText) findViewById(R.id.reg_bus_editText2);
		bus_editText3 = (TextView) findViewById(R.id.reg_bus_editText3);
		bus_editText4 = (TextView) findViewById(R.id.reg_bus_editText4);
		bus_editText5 = (EditText) findViewById(R.id.reg_bus_editText5);
		//
		lunch_editText1 = (EditText) findViewById(R.id.reg_lunch_editText1);
		lunch_editText2 = (EditText) findViewById(R.id.reg_lunch_editText2);
		lunch_editText3 = (TextView) findViewById(R.id.reg_lunch_editText3);
		lunch_editText4 = (TextView) findViewById(R.id.reg_lunch_editText4);
		lunch_editText5 = (EditText) findViewById(R.id.reg_lunch_editText5);
		//
		res_editText1 = (EditText) findViewById(R.id.reg_res_editText6);
		res_editText2 = (EditText) findViewById(R.id.reg_res_editText7);
		res_editText3 = (TextView) findViewById(R.id.reg_res_editText8);
		res_editText4 = (TextView) findViewById(R.id.reg_res_editText9);
		res_editText5 = (EditText) findViewById(R.id.reg_res_editText10);
	}

	private void updateDisplay_hotel1() {
		mDateDisplay_hotel1.setText(new StringBuilder().append(mYear)
				.append("-").append(pad(mMonth + 1)).append("-")
				.append(pad(mDay)).append(" "));
	}

	private void updateDisplay_hotel2() {
		mDateDisplay_hotel2.setText(new StringBuilder().append(mYear)
				.append("-").append(pad(mMonth + 1)).append("-")
				.append(pad(mDay)).append(" "));
	}

	private void updateDisplay_bus1() {
		mDateDisplay_bus1.setText(new StringBuilder().append(mYear).append("-")
				.append(pad(mMonth + 1)).append("-").append(pad(mDay))
				.append(" "));
	}

	private void updateDisplay_bus2() {
		mDateDisplay_bus2.setText(new StringBuilder().append(mYear).append("-")
				.append(pad(mMonth + 1)).append("-").append(pad(mDay))
				.append(" "));
	}

	private void updateDisplay_packed1() {
		mDateDisplay_packed1.setText(new StringBuilder().append(mYear)
				.append("-").append(pad(mMonth + 1)).append("-")
				.append(pad(mDay)).append(" "));
	}

	private void updateDisplay_packed2() {
		mDateDisplay_packed2.setText(new StringBuilder().append(mYear)
				.append("-").append(pad(mMonth + 1)).append("-")
				.append(pad(mDay)).append(" "));
	}

	private void updateDisplay_res1() {
		mDateDisplay_res1.setText(new StringBuilder().append(mYear).append("-")
				.append(pad(mMonth + 1)).append("-").append(pad(mDay))
				.append(" "));
	}

	private void updateDisplay_res2() {
		mDateDisplay_res2.setText(new StringBuilder().append(mYear).append("-")
				.append(pad(mMonth + 1)).append("-").append(pad(mDay))
				.append(" "));
	}

	private static String pad(int c) {
		if (c >= 10) {
			return String.valueOf(c);
		} else
			return "0" + String.valueOf(c);
	}

	private DatePickerDialog.OnDateSetListener dateSetListener1 = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDisplay_hotel1();
		}
	};

	private DatePickerDialog.OnDateSetListener dateSetListener2 = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDisplay_hotel2();
		}
	};

	private DatePickerDialog.OnDateSetListener dateSetListener3 = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDisplay_bus1();
		}
	};

	private DatePickerDialog.OnDateSetListener dateSetListener4 = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDisplay_bus2();
		}
	};

	private DatePickerDialog.OnDateSetListener dateSetListener5 = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDisplay_packed1();
		}
	};

	private DatePickerDialog.OnDateSetListener dateSetListener6 = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDisplay_packed2();
		}
	};

	private DatePickerDialog.OnDateSetListener dateSetListener7 = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDisplay_res1();
		}
	};

	private DatePickerDialog.OnDateSetListener dateSetListener8 = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDisplay_res2();
		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (DATE_DIALOG_ID) {
		case 1:
			return new DatePickerDialog(this, dateSetListener1, mYear, mMonth,
					mDay);
		case 2:
			return new DatePickerDialog(this, dateSetListener2, mYear, mMonth,
					mDay);
		case 3:
			return new DatePickerDialog(this, dateSetListener3, mYear, mMonth,
					mDay);
		case 4:
			return new DatePickerDialog(this, dateSetListener4, mYear, mMonth,
					mDay);
		case 5:
			return new DatePickerDialog(this, dateSetListener5, mYear, mMonth,
					mDay);
		case 6:
			return new DatePickerDialog(this, dateSetListener6, mYear, mMonth,
					mDay);
		case 7:
			return new DatePickerDialog(this, dateSetListener7, mYear, mMonth,
					mDay);
		case 8:
			return new DatePickerDialog(this, dateSetListener8, mYear, mMonth,
					mDay);
		}
		return null;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_registerbar, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_done) {

			pDialog = new ProgressDialog(InfoRegistPage.this,
					ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);
			pDialog.setMessage("등록 중...");
			// pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();

			new Thread(new Runnable() {
				public void run() {
					login();
				}
			}).start();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	void login() {
		try {

			httpclient = new DefaultHttpClient();
			if (res_editText5.getText().toString().startsWith("auto")) {
				httppost = new HttpPost(
						"http://s940217.mireene.com/dummy_register.php");
			} else
				httppost = new HttpPost(
						"http://s940217.mireene.com/travel_register.php");

			nameValuePairs = new ArrayList<NameValuePair>(33);// 8*4 + 4 + 1

			SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
			String username = pref.getString("username", "");

			nameValuePairs.add(new BasicNameValuePair("username", username));
			//
			nameValuePairs.add(new BasicNameValuePair("hotel_editText1",
					hotel_editText1.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("hotel_editText2",
					hotel_editText2.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("hotel_editText3",
					hotel_editText3.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("hotel_editText4",
					hotel_editText4.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("hotel_editText5",
					hotel_editText5.getText().toString()));
			//
			nameValuePairs.add(new BasicNameValuePair("bus_editText1",
					bus_editText1.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("bus_editText2",
					bus_editText2.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("bus_editText3",
					bus_editText3.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("bus_editText4",
					bus_editText4.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("bus_editText5",
					bus_editText5.getText().toString()));
			//
			nameValuePairs.add(new BasicNameValuePair("lunch_editText1",
					lunch_editText1.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("lunch_editText2",
					lunch_editText2.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("lunch_editText3",
					lunch_editText3.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("lunch_editText4",
					lunch_editText4.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("lunch_editText5",
					lunch_editText5.getText().toString()));
			//
			nameValuePairs.add(new BasicNameValuePair("res_editText1",
					res_editText1.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("res_editText2",
					res_editText2.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("res_editText3",
					res_editText3.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("res_editText4",
					res_editText4.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("res_editText5",
					res_editText5.getText().toString()));
			//
			// ////////////////////////////////////////////////////////////////////////
			nameValuePairs.add(new BasicNameValuePair("hotel_spin1",
					spin_hotel_location.getSelectedItem().toString()));

			nameValuePairs.add(new BasicNameValuePair("hotel_spin2",
					spin_hotel_locationgu.getSelectedItem().toString()));

			nameValuePairs.add(new BasicNameValuePair("hotel_spin3", spin_hotel
					.getSelectedItem().toString()));
			//
			nameValuePairs.add(new BasicNameValuePair("bus_spin1",
					spin_bus_location_from.getSelectedItem().toString()));

			nameValuePairs.add(new BasicNameValuePair("bus_spin2",
					spin_bus_location_to.getSelectedItem().toString()));

			nameValuePairs.add(new BasicNameValuePair("bus_spin3", spin_bus
					.getSelectedItem().toString()));
			//
			nameValuePairs.add(new BasicNameValuePair("lunch_spin1",
					spin_packed_location.getSelectedItem().toString()));

			nameValuePairs.add(new BasicNameValuePair("lunch_spin2",
					spin_packed_locationgu.getSelectedItem().toString()));

			nameValuePairs.add(new BasicNameValuePair("lunch_spin3",
					spin_packed.getSelectedItem().toString()));
			//
			nameValuePairs.add(new BasicNameValuePair("res_spin1",
					spin_res_location.getSelectedItem().toString()));

			nameValuePairs.add(new BasicNameValuePair("res_spin2",
					spin_res_locationgu.getSelectedItem().toString()));

			nameValuePairs.add(new BasicNameValuePair("res_spin3", spin_res
					.getSelectedItem().toString()));

			// ///////////////////////////////////////////////////////////////////////
			nameValuePairs.add(new BasicNameValuePair("checkbox_hotel", String
					.valueOf((checkbox_hotel.isChecked()) ? 1 : 0)));
			System.out.println("checkbox_hotel: "
					+ String.valueOf((checkbox_hotel.isChecked()) ? 1 : 0));
			nameValuePairs.add(new BasicNameValuePair("checkbox_bus", String
					.valueOf((checkbox_bus.isChecked()) ? 1 : 0)));
			System.out.println("checkbox_bus: "
					+ String.valueOf((checkbox_bus.isChecked()) ? 1 : 0));

			nameValuePairs.add(new BasicNameValuePair("checkbox_packed", String
					.valueOf((checkbox_packed.isChecked()) ? 1 : 0)));
			System.out.println("checkbox_packed: "
					+ String.valueOf((checkbox_packed.isChecked()) ? 1 : 0));

			nameValuePairs.add(new BasicNameValuePair("checkbox_res", String
					.valueOf((checkbox_res.isChecked()) ? 1 : 0)));
			System.out.println("checkbox_res: "
					+ String.valueOf((checkbox_res.isChecked()) ? 1 : 0));

			// ///////////////////////////////////////////////////////////////////////
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
					HTTP.UTF_8));
			// Execute HTTP Post Request
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			final String response = httpclient.execute(httppost,
					responseHandler);

			System.out.println("Response : " + response);
			runOnUiThread(new Runnable() {
				public void run() {
					// tv.setText("Response from PHP : " + response);
					System.out.println(response);
					pDialog.dismiss();
				}
			});

			if (response.equalsIgnoreCase("Sign Up")) {
				setResult(1001);
				// Create a new thread inside your Actvity.
				waitThread();

				finish();
			}

			else {
				showAlert();
			}

		} catch (Exception e) {
			pDialog.dismiss();
			System.out.println("Exception : " + e.getMessage());
		}
	}
	
	void waitThread() {
		Thread thread = new Thread() {

			@Override
			public void run() {
				// Block this thread for 2 seconds.
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if(notint < 5) {
							showNoti(noti_str[notint]);
							notint++;
							waitThread();
						}
					}
				});

			}

		};

		// Don't forget to start the thread.
		thread.start();
	}
	
	void showNoti(String str) {
		mNM = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		mNoti = new NotificationCompat.Builder(
				getBaseContext())
				.setContentTitle("Picknic")
				.setContentText(str)
				.setSmallIcon(R.drawable.ic_launcher)
				.setTicker(str).build();

		mNM.notify(7777+notint, mNoti);
	}
	
	
	public void showAlert() {
		InfoRegistPage.this.runOnUiThread(new Runnable() {
			public void run() {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						InfoRegistPage.this,
						ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);
				builder.setTitle("Login Error.");
				builder.setMessage("다시 시도해 주시기 바랍니다.")
						.setCancelable(false)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
			}
		});
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (pDialog != null) {
			pDialog.dismiss();
			pDialog = null;
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub

	}

}
