package com.example.danpoong.regist;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import com.example.danpoong.R;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class RegisterPage extends Activity implements OnItemSelectedListener {

	EditText registerStoreName, registerStoreCeo, registerStoreTel,
			registerStoreLocation, registerStoreKind, registerStoreSn;

	private String[] StoreAddress = { "서울", "경기", "인천", "부산", "대구", "광주", "대전",
			"울산", "강원", "경남", "경북", "전남", "전북", "충남", "충북", "제주" };
	private String[] JobCategory = { "숙박", "버스", "도시락", "식사" };
	private String[] StartTime = { "00:00", "1:00", "2:00", "3:00", "4:00",
			"5:00", "6:00", "7:00", "8:00", "9:00", "10:00", "11:00", "12:00",
			"13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00",
			"20:00", "21:00", "22:00", "23:00", "24:00" };
	private String[] EndTime = { "00:00", "1:00", "2:00", "3:00", "4:00",
			"5:00", "6:00", "7:00", "8:00", "9:00", "10:00", "11:00", "12:00",
			"13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00",
			"20:00", "21:00", "22:00", "23:00", "24:00" };

	HttpPost httppost;
	StringBuffer buffer;
	HttpResponse response;
	HttpClient httpclient;
	List<NameValuePair> nameValuePairs;
	ProgressDialog dialog;
	Spinner spin;
	Button signup_button;
	//
	Spinner register_page_spinner1, register_page_spinner2,
			register_page_spinner3, register_page_spinner4;
	//
	EditText register_page_editText1, register_page_editText2,
			register_page_editText3, register_page_editText4,
			register_page_editText5, register_page_editText6,
			register_page_editText7, register_page_editText8,
			register_page_editText9, register_page_editText10;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_regist);

		register_page_spinner1 = (Spinner) findViewById(R.id.register_page_spinner1);
		register_page_spinner2 = (Spinner) findViewById(R.id.register_page_spinner2);
		register_page_spinner3 = (Spinner) findViewById(R.id.register_page_spinner3);
		register_page_spinner4 = (Spinner) findViewById(R.id.register_page_spinner4);

		register_page_spinner1.setOnItemSelectedListener(this); // 액티비티 스스로를 선택
																// 변경
		// 이벤트에 대한 리스너로 등록
		register_page_spinner2.setOnItemSelectedListener(this);
		register_page_spinner3.setOnItemSelectedListener(this);
		register_page_spinner4.setOnItemSelectedListener(this);

		ArrayAdapter<String> address = new ArrayAdapter<String>(this,
				R.layout.regist_style, StoreAddress);
		ArrayAdapter<String> job = new ArrayAdapter<String>(this,
				R.layout.regist_style, JobCategory);

		ArrayAdapter<String> start = new ArrayAdapter<String>(this,
				R.layout.regist_time_style, StartTime);
		ArrayAdapter<String> end = new ArrayAdapter<String>(this,
				R.layout.regist_time_style, EndTime);

		// 드롭다운 화면에 표시
		address.setDropDownViewResource(R.layout.regist_style);
		register_page_spinner1.setAdapter(address);
		job.setDropDownViewResource(R.layout.regist_style);
		register_page_spinner2.setAdapter(job);
		start.setDropDownViewResource(R.layout.regist_time_style);
		register_page_spinner3.setAdapter(start);
		end.setDropDownViewResource(R.layout.regist_time_style);
		register_page_spinner4.setAdapter(end);

		// Action Bar 설정
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.actionbar_yellow_bg));
		actionBar.setDisplayHomeAsUpEnabled(false); // back버튼 안 나오게
		actionBar.setDisplayShowHomeEnabled(false); // 기본로고 OFF

		// //////////////////////////////////////////////////////////////////////
		register_page_editText1 = (EditText) findViewById(R.id.register_page_editText1);
		register_page_editText2 = (EditText) findViewById(R.id.register_page_editText2);
		register_page_editText3 = (EditText) findViewById(R.id.register_page_editText3);
		register_page_editText4 = (EditText) findViewById(R.id.register_page_editText4);
		register_page_editText5 = (EditText) findViewById(R.id.register_page_editText5);
		register_page_editText6 = (EditText) findViewById(R.id.register_page_editText6);
		register_page_editText7 = (EditText) findViewById(R.id.register_page_editText7);
		register_page_editText8 = (EditText) findViewById(R.id.register_page_editText8);
		register_page_editText9 = (EditText) findViewById(R.id.register_page_editText9);
		register_page_editText10 = (EditText) findViewById(R.id.register_page_editText10);

		// //////////////////////////////////////////////////////////////////////

		signup_button = (Button) findViewById(R.id.regist_store_button1);
		signup_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dialog = new ProgressDialog(RegisterPage.this,
						ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);
				dialog.setMessage("회원가입 중...");
				// pDialog.setIndeterminate(false);
				dialog.setCancelable(true);
				dialog.show();

				new Thread(new Runnable() {
					public void run() {
						login();
					}
				}).start();
			}

		});
		//

	}

	// ActionBar & (res>menu>activity_registbar.xml) 연결
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub

	}

	void login() {
		try {

			httpclient = new DefaultHttpClient();
			httppost = new HttpPost("http://s940217.mireene.com/signup_store.php");

			//httppost.setEntity();
			// add your data
			nameValuePairs = new ArrayList<NameValuePair>(9);// 8+1
			//httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.)); 

			// Always use the same variable name for posting i.e the android
			// side variable name and php side variable name should be similar,
			Intent intentfromto = getIntent();
			String email = intentfromto.getStringExtra("email_address");
			Log.d("RegisterPage", email);
			
			nameValuePairs.add(new BasicNameValuePair("username", email));
			
			String store_name = register_page_editText1.getText().toString().replaceAll("\\p{Z}", "");
			nameValuePairs.add(new BasicNameValuePair("edittext1",store_name
					));
			
			nameValuePairs.add(new BasicNameValuePair("edittext2",
					register_page_editText2.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("edittext3",
					register_page_editText3.getText().toString() + "-"
							+ register_page_editText4.getText().toString()
							+ "-"
							+ register_page_editText5.getText().toString()));
			
			nameValuePairs.add(new BasicNameValuePair("edittext4", register_page_spinner1.getSelectedItem().toString() + " "
					+ register_page_editText6.getText().toString()
					));

			nameValuePairs.add(new BasicNameValuePair("edittext5",
					register_page_spinner2.getSelectedItem().toString()));

			nameValuePairs.add(new BasicNameValuePair("edittext6",
					register_page_editText7.getText().toString() + "-"
							+ register_page_editText8.getText().toString()
							+ "-"
							+ register_page_editText9.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("edittext7",
					register_page_spinner3.getSelectedItem().toString()
							+ "~"
							+ register_page_spinner4.getSelectedItem()
									.toString()));

			nameValuePairs.add(new BasicNameValuePair("edittext8",
					register_page_editText10.getText().toString()));
			
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
			// Execute HTTP Post Request
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			final String response = httpclient.execute(httppost,
					responseHandler);

			// ResponseHandler<String> responseHandler = new
			// BasicResponseHandler();
			// final String response = httpclient.execute(httppost,
			// responseHandler);
			System.out.println("Response : " + response);
			runOnUiThread(new Runnable() {
				public void run() {
					// tv.setText("Response from PHP : " + response);
					System.out.println(response);
					dialog.dismiss();
				}
			});

			if (response.equalsIgnoreCase("Sign Up")) {
				runOnUiThread(new Runnable() {
					public void run() {
						Toast.makeText(RegisterPage.this, "Sign up Success",
								Toast.LENGTH_SHORT).show();
						finish();
					}
				});

			} else {
				showAlert();
			}

		} catch (Exception e) {
			dialog.dismiss();
			System.out.println("Exception : " + e.getMessage());
		}
	}
	
	
	public void showAlert() {
		RegisterPage.this.runOnUiThread(new Runnable() {
			public void run() {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						RegisterPage.this,
						ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);
				builder.setTitle("Login Error.");
				builder.setMessage("다시 시도해 주시기 바랍니다.")
						.setCancelable(false)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
			}
		});
	}

}
