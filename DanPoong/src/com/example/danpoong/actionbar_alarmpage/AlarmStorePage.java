package com.example.danpoong.actionbar_alarmpage;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.danpoong.R;
//import android.support.v4.widget.SwipeRefreshLayout;

public class AlarmStorePage extends ListActivity //implements SwipeRefreshLayout.OnRefreshListener
{
	//SharedPreferences
	SharedPreferences pref;
	int is_store;
	
	ListView alarm_listview;
	private String actionBarTitle="�˸�";
//	SwipeRefreshLayout refreshLayout;
	

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_store);
        
		//SharedPreferences
		pref = getSharedPreferences("pref", MODE_PRIVATE);
		is_store = pref.getInt("is_store", 0);
		
		//ActionBar ����
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_yellow_bg));
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setTitle(actionBarTitle);
		
        ArrayList<Alarm> m_orders = new ArrayList<Alarm>();
        
        // ����Ʈ�� �߰��� ��ü
        Alarm a1 = new Alarm("�̸���ѹ������", "����");
        Alarm a2 = new Alarm("�δ������", "����");
        Alarm a3 = new Alarm("���츮 ������", "����");
        Alarm a4 = new Alarm("���ѱ��� �б�����", "����");
        Alarm a5 = new Alarm("����Ƽ��", "����");
        Alarm a6 = new Alarm("Ȳ��", "����");
        Alarm a7 = new Alarm("���̼����� ������", "����");
        Alarm a8 = new Alarm("���Ѱ��⼱������", "����");
        Alarm a9 = new Alarm("Ŵ���׸�", "����");
        Alarm a10 = new Alarm("�ջﵷ", "����");
        
        // ����Ʈ�� ��ü�� �߰�
        m_orders.add(a1);
        m_orders.add(a2);
        m_orders.add(a3);
        m_orders.add(a4);
        m_orders.add(a5);
        m_orders.add(a6);
        m_orders.add(a7);
        m_orders.add(a8);
        m_orders.add(a9);
        m_orders.add(a10);
        
        // ����� ����
        AlarmAdapter m_adapter = new AlarmAdapter(this, R.layout.list_alarm_store, m_orders);
        setListAdapter(m_adapter);
//        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swype_alarm);
//		refreshLayout.setOnRefreshListener(this);
//		refreshLayout.setColorScheme(android.R.color.holo_blue_bright, 
//	            android.R.color.holo_green_light, 
//	            android.R.color.holo_orange_light, 
//	            android.R.color.holo_red_light);
        
    }
    private class AlarmAdapter extends ArrayAdapter<Alarm> {
    	
    	private ArrayList<Alarm> items;
    	
    	public AlarmAdapter(Context context, int textViewResourceId, ArrayList<Alarm> items) {
    		super(context, textViewResourceId, items);
    		this.items = items;
    	}
    	
    	@Override
    	public View getView(int position, View convertView, ViewGroup parent) {
    		View v = convertView;
    		if(v == null) {
    			LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    			v = vi.inflate(R.layout.list_alarm_store, null);
    		}
    		Alarm a = items.get(position);
    		if (a != null) {
    			TextView tt = (TextView) v.findViewById(R.id.alarm_store_toptext);
    			TextView bt = (TextView) v.findViewById(R.id.alarm_store_bottomtext);
    			if (tt != null){
    				tt.setText(a.getTitle() + " �Բ��� ȸ������ ���Ը� �����Ͽ����ϴ�.");
    			}
    			if(bt != null){
    				bt.setText(a.getTime());
    			}
    		}
    		return v;
    	}
    }
    
    class Alarm {
    	private String Title;
    	private String Time;
    	
    	public Alarm(String _Title, String _Time){
    		this.Title = _Title;
    		this.Time = _Time;
    	}
    	public String getTitle() {
    		return Title;
    	}
    	public String getTime(){
    		return Time;
    	}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
//   @Override
//	public void onRefresh(){
		//refresh
//		new Handler().postDelayed(new Runnable() {
//	        @Override public void run() {
//	            refreshLayout.setRefreshing(false);
//	        }
//	    }, 3000);
//	}
}
