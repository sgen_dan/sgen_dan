package com.example.danpoong.actionbar_alarmpage;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.danpoong.R;
//import android.support.v4.widget.SwipeRefreshLayout;
import com.example.danpoong.list.ListItem;

public class AlarmPage extends ListActivity implements SwipeRefreshLayout.OnRefreshListener
{
	//SharedPreferences
	SharedPreferences pref;
	int is_store;
	
	ListView alarm_listview;
	private String actionBarTitle="알림";
	SwipeRefreshLayout refreshLayout;
	AlarmAdapter m_adapter;
	ArrayList<Alarm> m_orders;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        
		//SharedPreferences
		pref = getSharedPreferences("pref", MODE_PRIVATE);
		is_store = pref.getInt("is_store", 0);
		
		//ActionBar 설정
		ActionBar actionBar = getActionBar();
		if(is_store==1) {
			actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_yellow_bg));
		}
		actionBar.setDisplayShowHomeEnabled(false); //기본로고 OFF
		actionBar.setHomeButtonEnabled(true); //뒤로가기 버튼 ON

		actionBar.setTitle(actionBarTitle);
		
         m_orders = new ArrayList<Alarm>();
        
        // 리스트에 추가할 객체
//        Alarm a1 = new Alarm("이모네한방왕족발", "지금");
//        Alarm a2 = new Alarm("부대찌개삼겹살", "지금");
//        Alarm a3 = new Alarm("봉우리 역삼점", "지금");
//        Alarm a4 = new Alarm("대한극장 압구정점", "지금");
//        Alarm a5 = new Alarm("리미티드", "지금");
//        Alarm a6 = new Alarm("황돈", "지금");
//        Alarm a7 = new Alarm("신촌설렁탕 강남점", "지금");
//        Alarm a8 = new Alarm("착한고기선릉역점", "지금");
//        Alarm a9 = new Alarm("킴스그릴", "지금");
//        Alarm a10 = new Alarm("왕삼돈", "지금");
        
        // 리스트에 객체를 추가
//        m_orders.add(a1);
//        m_orders.add(a2);
//        m_orders.add(a3);
//        m_orders.add(a4);
//        m_orders.add(a5);
//        m_orders.add(a6);
//        m_orders.add(a7);
//        m_orders.add(a8);
//        m_orders.add(a9);
//        m_orders.add(a10);
        
        // 어댑터 생성
        m_adapter = new AlarmAdapter(this, R.layout.list_alarm, m_orders);
        setListAdapter(m_adapter);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_alarm);
		refreshLayout.setOnRefreshListener(this);
		refreshLayout.setColorScheme(android.R.color.holo_blue_bright, 
	            android.R.color.holo_green_light, 
	            android.R.color.holo_orange_light, 
	            android.R.color.holo_red_light);
        
    }
    
    private class AlarmAdapter extends ArrayAdapter<Alarm> {
    	
    	private ArrayList<Alarm> items;
    	
    	public AlarmAdapter(Context context, int textViewResourceId, ArrayList<Alarm> items) {
    		super(context, textViewResourceId, items);
    		this.items = items;
    	}
    	
    	@Override
    	public View getView(int position, View convertView, ViewGroup parent) {
    		View v = convertView;
    		if(v == null) {
    			LayoutInflater vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    			v = vi.inflate(R.layout.list_alarm, null);
    		}
    		Alarm a = items.get(position);
    		if (a != null) {
    			TextView tt = (TextView) v.findViewById(R.id.alarm_toptext);
    			TextView bt = (TextView) v.findViewById(R.id.alarm_bottomtext);
    			if (tt != null){
    				tt.setText(a.getTitle() + " 매장에서 입찰하였습니다.");
    			}
    			if(bt != null){
    				bt.setText(a.getTime());
    			}
    		}
    		return v;
    	}
    }
    
    class Alarm {
    	private String Title;
    	private String Time;
    	
    	public Alarm(String _Title, String _Time){
    		this.Title = _Title;
    		this.Time = _Time;
    	}
    	public String getTitle() {
    		return Title;
    	}
    	public String getTime(){
    		return Time;
    	}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

  //뒤로가기 버튼이 바로 전 단계로 가도록 설정
  	public boolean onOptionsItemSelected(android.view.MenuItem item) {
  		switch (item.getItemId()) {
  		case android.R.id.home:
  			finish();
  			return true;
  		case R.id.action_settings:
  			return true;
  		}
  		return super.onOptionsItemSelected(item);
  	};
  	
   @Override
	public void onRefresh(){
	   new AsyncTaskParseJson().execute();
	}
    
    public class AsyncTaskParseJson extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			refreshLayout.setRefreshing(true);
			/*
			 * pDialog = new ProgressDialog(mContext);
			 * pDialog.setMessage("데이터 가져오는 중...");
			 * //pDialog.setIndeterminate(false); pDialog.setCancelable(true);
			 * pDialog.show();
			 */
		}

		@Override
		protected String doInBackground(String... arg0) {

			StringBuilder sb = new StringBuilder();

			try {
				
				String username = pref.getString("username", "");
			
				Log.d("meal", "username: "+ username);

				// http://s940217.mireene.com/get_register_hotel.php?location="전체"&type="전체"
				String surl = "http://s940217.mireene.com/get_alarm.php?username=";
				surl = surl
						+ java.net.URLEncoder.encode(new String(username
								.getBytes("UTF-8")));
			
				
				Log.d("username", surl);
				URL url = new URL(surl);
				// URL url = new
				// URL("http://mapnoti.zz.mu/getdata.php?lat=37.5745215&lng=127.0388250&range=1.5");
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				if (conn != null) {

					conn.setConnectTimeout(5000);
					// conn.setUseCaches(false);
					System.out.println("connect");
					System.out.println(String.valueOf(conn.getResponseCode())
							+ "\n" + String.valueOf(HttpURLConnection.HTTP_OK));

					if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
						BufferedReader br = new BufferedReader(
								new InputStreamReader(conn.getInputStream()));

						while (true) {
							String line = br.readLine();
							if (line == null)
								break;
							sb.append(line + "\n");
						}
						br.close();

					} else {

					}
					conn.disconnect();
				}
			} catch (Exception e) {
				// Log.d("hotel", "connection error: "+e.toString());
				//Toast.makeText(thos, e.toString(), Toast.LENGTH_SHORT)
				//.show();
			}

			String jsonString = sb.toString();
			Log.d("meal", "jsonString " + jsonString);
			if(jsonString.length() > 5) {
				try {
					if (!m_orders.isEmpty())
						m_orders.clear();

					JSONArray ja = new JSONArray(jsonString);
					Log.d("meal", "ja.length: " + ja.length());

					for (int i = 0; i < ja.length(); i++) {
						JSONObject jo = ja.getJSONObject(i);
						
						String store_name = jo.getString("store_name");
						String now_date = jo.getString("now_date");// @drawable/img_nature1

						m_orders.add(new Alarm(store_name, now_date));

					}
					// Log.d("hotel", ""+mArray.size());

				} catch (JSONException e) {
					
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(String strFromDoInBg) {
			// pDialog.dismiss();
			refreshLayout.setRefreshing(false); // Refresh 애니메이션 Stop
			Log.d("alarm", "onPostExecute");
			m_adapter.notifyDataSetChanged();
		}

	}
}
