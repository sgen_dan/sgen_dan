package com.example.danpoong.actionbar_setting;
import com.example.danpoong.R;
import com.example.danpoong.actionbar_mypage.MyPageActivity;
import com.example.danpoong.actionbar_mypage.MyPageActivity_Store;

import android.app.ActionBar;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class SettingPage extends Activity {

	//SharedPreferences
	SharedPreferences pref;
	int is_store;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		//SharedPreferences
		pref = getSharedPreferences("pref", MODE_PRIVATE);
		is_store = pref.getInt("is_store", 0);

		super.onCreate(savedInstanceState);

		if(is_store==1) setContentView(R.layout.activity_setting_store);
		else setContentView(R.layout.activity_setting);

		ActionBar actionBar = getActionBar();
		if(is_store==1) {
			actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_yellow_bg));
		}
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setHomeButtonEnabled(true);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	//뒤로가기 버튼이 바로 전 단계로 가도록 설정
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.action_settings:
			return true;
		}
		return super.onOptionsItemSelected(item);
	};

}
