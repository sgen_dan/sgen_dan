package com.example.danpoong;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.example.danpoong.login.SignupPage;
import com.example.danpoong.regist.RegisterPage;

public class QuestionPage extends Activity {

	Button questionTravel, questionStore;

	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_question);
		questionTravel = (Button)findViewById(R.id.question_button_travel);
		questionStore = (Button)findViewById(R.id.question_button_store);
		
		//단체여행을 준비신가요?
		questionTravel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				PageChange(SignupPage.class, false);
			}
		});
			
		//사장님이신가요?
		questionStore.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				PageChange(SignupPage.class, true);
			}
		});
	}
	
	void PageChange (Class goActionBarClass, Boolean isStore) {
		if (goActionBarClass!=null) {
			Intent intent = new Intent(QuestionPage.this, goActionBarClass);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.putExtra("isStoreMode", isStore);
			startActivity(intent);
			finish();
		}
	}

}
