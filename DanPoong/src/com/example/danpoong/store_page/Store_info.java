package com.example.danpoong.store_page;

import java.util.ArrayList;

import com.example.danpoong.R;
import com.example.danpoong.countlist.CountListItem;
import com.example.danpoong.countlist.ImageAdapter;
import com.example.danpoong.countlist.map.CountListMap;
import com.example.danpoong.imagefromurl.ImageLoader;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


public class Store_info extends Activity {

	private String storeLocaionMap;
	private LayoutInflater countlist_inflater;

	private Gallery storeGallery;
	AlertDialog imageDialog=null;

	private ArrayList<review_item> mArray;
	private Store_review_page_adapter adapter;
	Intent cnt_intent = getIntent();
	
	String getNameResource; //지도페이지로 넘겨주기위해 전역변수로 선언

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_store_info);

		//Intent 받아오기
		Intent intent = getIntent();
		getNameResource = intent.getStringExtra("storeNameResource");
		String getTypeResource = intent.getStringExtra("storeTypeResource");
		String getPriceResource = intent.getStringExtra("storePriceResource");
		String getMemoResource = intent.getStringExtra("storeMemoResource");
		final String getTelResource = intent.getStringExtra("storeTelResource");
		final String [] getImageResources = intent.getStringArrayExtra("imageResourcesArray");

		//ActionBar 설정
		ActionBar actionBar = getActionBar();
		actionBar.setTitle(getNameResource);
		actionBar.setDisplayShowHomeEnabled(false); //기본로고 OFF
		actionBar.setHomeButtonEnabled(true); //뒤로가기 버튼 ON

		//xml 연결 - 사용자 정보
		TextView itemName=(TextView)findViewById(R.id.upche_name);
		//xml 연결 - Call & Map
		TextView itemCall=(TextView)findViewById(R.id.upche_call); //string 필요
		TextView itemMap=(TextView)findViewById(R.id.upche_map);
		//xml 연결 - 업체 정보
		TextView itemPrice=(TextView)findViewById(R.id.upche_price);
		TextView itemMemo=(TextView)findViewById(R.id.upche_introduce);

		//받아온 정보 표시
		itemName.setText(getNameResource);
		itemPrice.setText(getPriceResource);
		itemMemo.setText(getMemoResource);

		//List 
		setReviewList();

		//Call
		itemCall.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(Store_info.this);

				//알림창의 속성 설정
				builder.setMessage("연결된 연락처로 전화 하시겠습니까?")	// 메세지 설정
				.setCancelable(false)	// 뒤로 버튼 클릭시 취소 가능 설정
				.setPositiveButton("확인", new DialogInterface.OnClickListener(){       
					// 확인 버튼 클릭시 설정
					public void onClick(DialogInterface dialog, int whichButton){
						Uri storeTelCall = Uri.parse("tel:"+getTelResource);
						call(storeTelCall);
					}
				})
				.setNegativeButton("취소", new DialogInterface.OnClickListener(){      
					// 취소 버튼 클릭시 설정
					public void onClick(DialogInterface dialog, int whichButton){
						dialog.cancel();
					}
				});
				AlertDialog dialog = builder.create();    // 알림창 객체 생성
				dialog.show();    // 알림창 띄우기
			}
		});

		//Map
		itemMap.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				map();
			}
		});

		//Gallery
		final ImageAdapter imgAdapter = new ImageAdapter(this, getImageResources);
		storeGallery = (Gallery)this.findViewById(R.id.upche_image_gallery);
		storeGallery.setAdapter(imgAdapter);
		storeGallery.setSelection(1);
		storeGallery.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,int _position, long id) {

				//Dialog 생성
				Dialog dialog = new Dialog(Store_info.this);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.countlist_gallery_image_big);
				ImageView image = (ImageView) dialog.findViewById(R.id.thumbnail_image);
				String onClickImage = getImageResources[_position];
				image.setBackground(CreateBitmapDrawable(imgAdapter, onClickImage));

				//Dialog 테두리 없애기
				dialog.getWindow().setBackgroundDrawable(null); //setBackgroundDrawable(null)을 하지 않으면 (1px)짜리 테두리가 생김

				// Show the dialog
				dialog.show();
			}
		});
	}
	
	//뒤로가기 버튼이 바로 전 단계로 가도록 설정
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	};
	
	//Bitmap -> Drawable
	private BitmapDrawable CreateBitmapDrawable (ImageAdapter imgAdapter, String imageResource){

		//스크린사이즈 받아오기
		Display display = this.getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int screenWidth = size.x;
		int screenHeight = size.y;

		//이미지 받아와서 사이즈 조정
		ImageLoader imgLoader = new ImageLoader(this.getApplicationContext());	
		Bitmap bitmap = imgLoader.getBitmap(imageResource);
		int bitmapHeight = bitmap.getHeight();
		int bitmapWidth = bitmap.getWidth();

		bitmapHeight = bitmapHeight*5;
		bitmapWidth = bitmapWidth*5;

		//이미지가 너무 클 경우
		while(bitmapHeight > (screenHeight-250) || bitmapWidth > (screenWidth- 250)) {
			bitmapHeight = bitmapHeight / 2;
			bitmapWidth = bitmapWidth / 2;
		}

		//새로운 비트맵 이미지 생성
		BitmapDrawable resizedBitmap = new BitmapDrawable(this.getResources(), 
				Bitmap.createScaledBitmap(bitmap, bitmapWidth, bitmapHeight, false));

		return resizedBitmap;
	}

	//업체에 전화 걸기
	private void call (Uri callUri) {
		try {
			//Intent callIntent = new Intent(Intent.ACTION_CALL); //바로 전화걸기
			Intent callIntent = new Intent(Intent.ACTION_DIAL); //전화번호 표시만 하기
			callIntent.setData(callUri);
			startActivity(callIntent);
		}
		catch (ActivityNotFoundException e) {
			Log.d("전화걸기","전화걸기 실패",e);
		}
	}

	//지도 띄우기
	private void map () {
		try {
			Intent mapIntent = new Intent(this, CountListMap.class);
			mapIntent.putExtra("storeNameOnMapActionBar", getNameResource);
			startActivity(mapIntent);
		}
		catch (ActivityNotFoundException e) {
			Log.d("지도띄우기","지도띄우기 실패");
		}
	}

	//Item 추가 - DB연동 필요
	private void addItem() {
		mArray = new ArrayList<review_item>();
		mArray.add(new review_item ("soshoso", "양도 푸짐하고 맛도 좋았어요. 기대 이상입니다."));
		mArray.add(new review_item ("daun125", "다음에도 방문하고 싶을 정도로 아주 만족스럽습니다."));
		mArray.add(new review_item ("syunghyo", "맛 없어요"));
		mArray.add(new review_item ("suin", "음식은 훌륭하지만 서비스가 별로입니다"));
		mArray.add(new review_item ("soyoung", "남자친구가 좋아하는 곳이라 오빠랑 자주 와요ㅎ"));
		mArray.add(new review_item ("hyunji", "완전 마시써용!! 짱짱!!"));
	}

	//List 생성
	private void setReviewList() {
		ListView listView = (ListView)findViewById(R.id.upche_listview1);
		addItem();
		adapter = new Store_review_page_adapter(getBaseContext(), mArray);
		adapter.notifyDataSetChanged();
		listView.setAdapter(adapter);	
	}
}
