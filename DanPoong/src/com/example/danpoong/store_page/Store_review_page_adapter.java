package com.example.danpoong.store_page;

import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.danpoong.R;

public class Store_review_page_adapter extends BaseAdapter{

	//SharedPreferences
	SharedPreferences pref;
	int is_store;

	private LayoutInflater review_item_inflater;
	private Context review_item_activity;
	private ArrayList<review_item> review_item_array_data;

	public Store_review_page_adapter (Context mContext, ArrayList<review_item> mArray) {
		this.review_item_activity=mContext;
		review_item_array_data=mArray;
		review_item_inflater = (LayoutInflater)review_item_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		//SharedPreferences
		pref = mContext.getSharedPreferences("pref", mContext.MODE_PRIVATE);
		is_store = pref.getInt("is_store", 0);
	}

	@Override
	public int getCount() {
		return review_item_array_data.size();
	}

	@Override
	public Object getItem(int position) {
		return review_item_array_data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView==null) {
			int resource=0;
			if(is_store==1) resource=R.layout.y_countlist_item;//y_up_che_page�� �ٲ����.
			else resource=R.layout.upche_review_item;
			convertView = review_item_inflater.inflate(resource, parent, false);
			
			
		}
		TextView customName=(TextView)convertView.findViewById(R.id.review_customer_name);
		TextView customDetail=(TextView)convertView.findViewById(R.id.review_text_detail);
		
		customName.setText(review_item_array_data.get(position).customer_Name);
		customDetail.setText(review_item_array_data.get(position).review_Memo);
		
		return convertView;
	}
}