package com.example.danpoong.countlist;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.danpoong.R;
import com.example.danpoong.countlist.map.CountListMap;
import com.example.danpoong.imagefromurl.ImageLoader;
import com.example.danpoong.store_page.Store_info;

public class CountListAdapter extends BaseAdapter{

	//SharedPreferences
	SharedPreferences pref;
	int is_store;

	private LayoutInflater countlist_inflater;
	private Context countlist_activity;
	private ArrayList<CountListItem> countlist_array_data;
	private Uri storeTelCall;
	private String storeLocaionMap;

	Gallery storeGallery;
	AlertDialog imageDialog=null;

	private static final float DEFAULT_HDIP_DENSITY_SCALE = 1.5f;

	public CountListAdapter (Context mContext, ArrayList<CountListItem> mArray) {
		this.countlist_activity=mContext;
		countlist_array_data=mArray;
		countlist_inflater = (LayoutInflater)countlist_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		//SharedPreferences
		pref = mContext.getSharedPreferences("pref", mContext.MODE_PRIVATE);
		is_store = pref.getInt("is_store", 0);
	}

	@Override
	public int getCount() {
		return countlist_array_data.size();
	}

	@Override
	public Object getItem(int position) {
		return countlist_array_data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView==null) {
			int resource=0;
			if(is_store==1) resource=R.layout.y_countlist_item;
			else resource=R.layout.countlist_item;
			convertView = countlist_inflater.inflate(resource, parent, false);
		}

		//xml 연결 - 사용자 정보
		ImageView itemImg=(ImageView)convertView.findViewById(R.id.item_store_image);
		TextView itemName=(TextView)convertView.findViewById(R.id.item_store_name);
		TextView itemUpdate=(TextView)convertView.findViewById(R.id.item_store_update);
		//xml 연결 - Call & Map
		TextView itemCall=(TextView)convertView.findViewById(R.id.item_store_call); //string 필요
		TextView itemMap=(TextView)convertView.findViewById(R.id.item_store_map);
		//xml 연결 - 업체 정보
		TextView itemPrice=(TextView)convertView.findViewById(R.id.item_store_price);
		TextView itemMemo=(TextView)convertView.findViewById(R.id.item_store_memo);

		//data 넣기 - string data (array_data 연결)
		//사용자 정보
		itemName.setText(countlist_array_data.get(position).storeName);
		itemUpdate.setText(countlist_array_data.get(position).storeUpdate);

		//Call
		itemCall.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(countlist_activity);     // 여기서 this는 Activity의 this

				//알림창 속성 설정
				builder.setMessage("연결된 연락처로 전화 하시겠습니까?") // 메세지 설정
				.setCancelable(true)  // 뒤로 버튼 클릭시 취소 가능 설정
				.setPositiveButton("확인", new DialogInterface.OnClickListener(){       
					public void onClick(DialogInterface dialog, int whichButton){
						storeTelCall = Uri.parse("tel:"+countlist_array_data.get(position).storeTel);
						call(position, storeTelCall);
					}
				})
				.setNegativeButton("취소", new DialogInterface.OnClickListener(){      
					public void onClick(DialogInterface dialog, int whichButton){
						dialog.cancel();
					}
				});
				AlertDialog dialog = builder.create();    // 알림창 객체 생성
				dialog.show();    // 알림창 띄우기
			}
		});

		//Map
		itemMap.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				map(position);

			}
		});

		//User모드에서만 업체페이지 연결
		if(is_store==0) {
			//업체 페이지 연결
			itemName.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent page_intent = new Intent(countlist_activity,Store_info.class);
					page_intent.putExtra("storeNameResource", countlist_array_data.get(position).storeName);
					page_intent.putExtra("storeTelResource", countlist_array_data.get(position).storeTel);
					page_intent.putExtra("storeTypeResource", countlist_array_data.get(position).storeType);
					page_intent.putExtra("storePriceResource", countlist_array_data.get(position).storePrice);
					page_intent.putExtra("storeMemoResource", countlist_array_data.get(position).storeMemo);
					page_intent.putExtra("imageResourcesArray", countlist_array_data.get(position).storeImageResource);
					countlist_activity.startActivity(page_intent);
				}
			});
		}

		//업체정보
		itemPrice.setText(countlist_array_data.get(position).storePrice);
		itemMemo.setText(countlist_array_data.get(position).storeMemo);

		//Gallery
		final ImageAdapter imgAdapter = new ImageAdapter(countlist_activity, countlist_array_data.get(position).storeImageResource);
		storeGallery = (Gallery)convertView.findViewById(R.id.gallery_test);
		storeGallery.setAdapter(imgAdapter);
		storeGallery.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,int _position, long id) {

				//Dialog 생성
				Dialog dialog = new Dialog(countlist_activity);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.countlist_gallery_image_big);
				ImageView image = (ImageView) dialog.findViewById(R.id.thumbnail_image);
				image.setBackground(CreateBitmapDrawable(imgAdapter, _position));

				//Dialog 테두리 없애기
				dialog.getWindow().setBackgroundDrawable(null); //setBackgroundDrawable(null)을 하지 않으면 (1px)짜리 테두리가 생김

				// Show the dialog
				dialog.show();
			}
		});
		storeGallery.setSelection(1);

		return convertView;
	}

	private BitmapDrawable CreateBitmapDrawable (ImageAdapter imgAdapter, int position){

		//스크린사이즈 받아오기
		Display display = ((Activity) countlist_activity).getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int screenWidth = size.x;
		int screenHeight = size.y;
		System.out.println("w======"+screenWidth);
		System.out.println("H======"+screenHeight);

		//이미지 받아와서 사이즈 조정
		ImageLoader imgLoader = new ImageLoader(countlist_activity.getApplicationContext());	
		Bitmap bitmap = imgLoader.getBitmap(imgAdapter.imageUrl[position]);
		int bitmapHeight = bitmap.getHeight();
		int bitmapWidth = bitmap.getWidth();

		//이미지 사이즈 조절
		//bitmapHeight = bitmapHeight*5;
		//bitmapWidth = bitmapWidth*5;
		
		//이미지 사이즈가 너무 큰 경우, 축소
//		if (bitmapHeight > (screenHeight-300) || bitmapWidth > (screenWidth-250)) {
//			int iHeight=bitmapHeight;
//			int iWidth=bitmapWidth;
//			while(bitmapHeight > (screenHeight-250) || bitmapWidth > (screenWidth- 50)) {
//				bitmapHeight = bitmapHeight+iHeight;
//				bitmapWidth = bitmapWidth+iWidth;
//			}
//		}
		//이미지 사이즈가 작은 경우, 확대
		if (bitmapHeight < (screenHeight-500) || bitmapWidth < (screenWidth-500)) {
			int jHeight=bitmapHeight;
			int jWidth=bitmapWidth;
			while (bitmapHeight < (screenHeight-500) || bitmapWidth < (screenWidth-500)) {
				bitmapHeight=bitmapHeight+jHeight;
				bitmapWidth=bitmapWidth+jWidth;
			}
		}
		System.out.println("reW==="+bitmapWidth);
		System.out.println("reH==="+bitmapHeight);

		//새로운 비트맵 이미지 생성
		BitmapDrawable resizedBitmap = new BitmapDrawable(countlist_activity.getResources(), 
				Bitmap.createScaledBitmap(bitmap, bitmapWidth, bitmapHeight, false));

		return resizedBitmap;
	}

	private int PixelToDp(int pixel)
	{
		float scale = countlist_activity.getResources().getDisplayMetrics().density;

		return (int)(pixel / DEFAULT_HDIP_DENSITY_SCALE * scale);
	}

	public static Drawable BitmapToDrawable(Bitmap bitmap) {

		Drawable d = new BitmapDrawable(bitmap);
		return d;

	}		

	//전화 걸기
	private void call (int position, Uri callUri) {
		try {
			Intent callIntent = new Intent(Intent.ACTION_CALL); //바로 전화걸기
			//Intent callIntent = new Intent(Intent.ACTION_DIAL); //전화번호 표시만 하기
			callIntent.setData(callUri);
			countlist_activity.startActivity(callIntent);
		}
		catch (ActivityNotFoundException e) {
			Log.d("전화걸기","전화걸기 실패",e);
		}
	}

	//지도 띄우기
	private void map (int position) {
		try {
			Intent mapIntent = new Intent(countlist_activity, CountListMap.class); //전화번호 표시만 하기
			mapIntent.putExtra("storeNameOnMapActionBar", countlist_array_data.get(position).storeName);
			countlist_activity.startActivity(mapIntent);
		}
		catch (ActivityNotFoundException e) {
			Log.d("지도띄우기","지도띄우기 실패");
		}
	}
}
