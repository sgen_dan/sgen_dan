package com.example.danpoong.countlist;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

import com.example.danpoong.R;
import com.example.danpoong.imagefromurl.ImageLoader;

public class ImageAdapter extends BaseAdapter{

	private Context mContext;
	String[] imageUrl; 
	ImageView imageView;

	public ImageAdapter (Context mContext, String[] imageUrl) {
		this.mContext=mContext;
		this.imageUrl = imageUrl;
	}

	@Override
	public int getCount() {
		return imageUrl.length;
	}

	@Override
	public Object getItem(int position) {
		return imageUrl[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		// Loader image - will be shown before loading image
		int loader = R.drawable.no_media;

		if (convertView==null){
			imageView=new ImageView(mContext);
			imageView.setLayoutParams(new Gallery.LayoutParams(500, 500));
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		}
		else {
			imageView=(ImageView)convertView;
		}

		// ImageLoader class instance
		ImageLoader imgLoader = new ImageLoader(mContext.getApplicationContext());

		// whenever you want to load an image from url
		// call DisplayImage function
		// url - image url to load
		// loader - loader image, will be displayed before getting image
		// image - ImageView 
		imgLoader.DisplayImage(imageUrl[position], loader, imageView);

		return imageView;
	}

	public ImageView getImageView (int position) {
		// Loader image - will be shown before loading image
		
		int loader = R.drawable.no_media;

		imageView=new ImageView(mContext);
		imageView.setLayoutParams(new Gallery.LayoutParams(500, 500));
		imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

		// ImageLoader class instance
		ImageLoader imgLoader = new ImageLoader(mContext.getApplicationContext());

		// whenever you want to load an image from url
		// call DisplayImage function
		// url - image url to load
		// loader - loader image, will be displayed before getting image
		// image - ImageView 
		imgLoader.DisplayImage(imageUrl[position], loader, imageView);

		return imageView;
	}

}
