package com.example.danpoong.countlist;

import java.io.Serializable;

public class CountListItem implements Serializable{

	public String storeName, storeUpdate;
	public String storeTel, storeLocation;
	public String storeType;
	public String storePrice, storeMemo;
	public String[] storeImageResource;

	public CountListItem(String _storeName, String _storeUpdate, String _storeTel, 
			String _storeLocation, String _storeType, String _storePrice, String _storeMemo, 
			int _storeImageNum, String[] _storeImageResource) {
		
		storeName=_storeName;
		storeUpdate=_storeUpdate;
		storeTel=_storeTel;
		storeLocation=_storeLocation;
		storeType=_storeType;
		storePrice=_storePrice;
		storeMemo=_storeMemo;
		storeImageResource=_storeImageResource;
	}
}