package com.example.danpoong.countlist.map;

import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import com.example.danpoong.R;
import com.nhn.android.maps.NMapActivity;
import com.nhn.android.maps.NMapController;
import com.nhn.android.maps.NMapOverlay;
import com.nhn.android.maps.NMapOverlayItem;
import com.nhn.android.maps.NMapView;
import com.nhn.android.maps.NMapView.OnMapStateChangeListener;
import com.nhn.android.maps.NMapView.OnMapViewTouchEventListener;
import com.nhn.android.maps.maplib.NGeoPoint;
import com.nhn.android.maps.nmapmodel.NMapError;
import com.nhn.android.maps.overlay.NMapPOIdata;
import com.nhn.android.mapviewer.overlay.NMapCalloutOverlay;
import com.nhn.android.mapviewer.overlay.NMapOverlayManager;
import com.nhn.android.mapviewer.overlay.NMapOverlayManager.OnCalloutOverlayListener;
import com.nhn.android.mapviewer.overlay.NMapPOIdataOverlay;

public class CountListMap extends NMapActivity implements OnMapStateChangeListener, OnMapViewTouchEventListener, OnCalloutOverlayListener {

	//SharedPreferences
	SharedPreferences pref;
	int is_store;

	//Basic Map
	public static final String API_KEY = "30c48e2fb139f237125005b2e5f61099";
	NMapView mMapView = null;
	NMapController mMapController = null;

	// 오버레이의 리소스를 제공하기 위한 객체
	NMapViewerResourceProvider mMapViewerResourceProvider = null;
	// 오버레이 관리자
	NMapOverlayManager mOverlayManager;

	String actionBarTitle;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//SharedPreferences
		pref = getSharedPreferences("pref", MODE_PRIVATE);
		is_store = pref.getInt("is_store", 0);

		//ActionBar Title - Intent에서 넘겨받기
		Intent getMapIntent = getIntent();
		actionBarTitle = getMapIntent.getStringExtra("storeNameOnMapActionBar");

		//ActionBar 설정
		ActionBar actionBar = getActionBar();
		if(is_store==1) {
			actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_yellow_bg));
		}
		actionBar.setDisplayShowHomeEnabled(false); //기본로고 OFF
		actionBar.setHomeButtonEnabled(true); //뒤로가기 버튼 ON
		actionBar.setTitle(actionBarTitle);

		Log.d("Tag", "onCreate TestMap");

		// create map view
		mMapView = new NMapView(this);

		// set a registered API key for Open MapViewer Library
		mMapView.setApiKey(API_KEY);

		// set the activity content to the map view
		setContentView(mMapView);

		// initialize map view
		mMapView.setClickable(true);

		// register listener for map state changes
		mMapView.setOnMapStateChangeListener(this);
		mMapView.setOnMapViewTouchEventListener(this);

		// use built in zoom controls
		mMapView.setBuiltInZoomControls(true, null);

		// use map controller to zoom in/out, pan and set map center, zoom level etc.
		mMapController = mMapView.getMapController();

		// 오버레이 리소스 관리객체 할당
		mMapViewerResourceProvider = new NMapViewerResourceProvider(this);

		// 오버레이 관리자 추가
		mOverlayManager = new NMapOverlayManager(this, mMapView, mMapViewerResourceProvider);

		// 오버레이들을 관리하기 위한 id값 생성
		int markerId = NMapPOIflagType.PIN;

		// 표시할 위치 데이터를 지정한다. 마지막 인자가 오버레이를 인식하기 위한 id값
		NMapPOIdata poiData = new NMapPOIdata(2, mMapViewerResourceProvider);
		poiData.beginPOIdata(2);
		poiData.addPOIitem(37.497359, 127.026796, "음식점 위치", markerId, 0);
		poiData.endPOIdata();

		// 위치 데이터를 사용하여 오버레이 생성
		NMapPOIdataOverlay poiDataOverlay 
		= mOverlayManager.createPOIdataOverlay(poiData, null);

		// id값이 0으로 지정된 모든 오버레이가 표시되고 있는 위치로
		// 지도의 중심과 ZOOM을 재설정
		poiDataOverlay.showAllPOIdata(0);

		// 오버레이 이벤트 등록(오류남-주석처리)
		mOverlayManager.setOnCalloutOverlayListener(this);
	}

	//뒤로가기 버튼이 바로 전 단계로 가도록 설정
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	};

	@Override
	public void onLongPress(NMapView arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onLongPressCanceled(NMapView arg0) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onScroll(NMapView arg0, MotionEvent arg1, MotionEvent arg2) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onSingleTapUp(NMapView arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onTouchDown(NMapView arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onTouchUp(NMapView arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onAnimationStateChange(NMapView arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onMapCenterChange(NMapView arg0, NGeoPoint arg1) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onMapCenterChangeFine(NMapView arg0) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onMapInitHandler(NMapView mapView, NMapError errorInfo) {
		//NGeoPoint 위치 전달방법 & 토큰 표시
		if (errorInfo == null) { 
			mMapController.setMapCenter(new NGeoPoint(126.978371, 37.5666091), 50);
		} 
		else {
			Log.e("NMAP", "onMapInitHandler: error=" + errorInfo.toString());
		}

	}
	@Override
	public void onZoomLevelChange(NMapView arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	//오버레이가 클릭되었을 때의 이벤트(오류나서 주석처리함) 
	@Override
	public NMapCalloutOverlay onCreateCalloutOverlay(NMapOverlay arg0, NMapOverlayItem arg1, Rect arg2) {
		Toast.makeText(this, actionBarTitle ,Toast.LENGTH_SHORT).show();
		return null;
	}

}