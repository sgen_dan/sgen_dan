package com.example.danpoong;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.danpoong.tab.BusTab;
import com.example.danpoong.tab.HotelTab;
import com.example.danpoong.tab.PackedLunchTab;
import com.example.danpoong.tab.RestaurantTab;

public class TabsPagerAdapter extends FragmentPagerAdapter {

	private Context mContext;
	
	//Main (isMain==true), MyPage (isMain==false) 
	private boolean isMain = true;
	
	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}
	
	public void setContext(Context mContext)
	{
		this.mContext = mContext;
	}
	
	//Tab �̵�
	@Override
	public Fragment getItem(int index) {
		switch (index) {
		case 0:
			return new HotelTab(mContext, isMain);
		case 1:
			return new BusTab(mContext, isMain);
		case 2:
			return new PackedLunchTab(mContext, isMain);
		case 3:
			return new RestaurantTab(mContext, isMain);
		}
		return null;
	}
	
	//Tab ����
	@Override
	public int getCount() {
		return 4;
	}
}
