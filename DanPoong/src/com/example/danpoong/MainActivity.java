package com.example.danpoong;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

import com.example.danpoong.actionbar_alarmpage.AlarmPage;
import com.example.danpoong.actionbar_mypage.MyPageActivity;
import com.example.danpoong.actionbar_mypage.MyPageActivity_Store;
import com.example.danpoong.actionbar_setting.SettingPage;
import com.example.danpoong.regist.InfoRegistPage;

public class MainActivity extends FragmentActivity implements ActionBar.TabListener,OnClickListener {

	//SharedPreferences
	SharedPreferences pref;
	int is_store;
	
	private ViewPager viewPager;
	private TabsPagerAdapter tabsPagerAdapter;
	private ActionBar actionBar;
	private Button joinButton;
	private NotificationManager mNM;
	private Notification mNoti;
	// Tab Title
	private int[] tabsIcon_nor = {R.drawable.ic_home_nor, R.drawable.ic_bus_nor, R.drawable.ic_lunchbox_nor, R.drawable.ic_restaurant_nor};
	private int[] tabsIcon_sel = {R.drawable.ic_home_sel, R.drawable.ic_bus_sel, R.drawable.ic_lunchbox_sel, R.drawable.ic_restaurant_sel};
	private int[] tabsIcon_sel_y = {R.drawable.y_ic_home_sel, R.drawable.y_ic_bus_sel, R.drawable.y_ic_lunchbox_sel, R.drawable.y_ic_restaurant_sel};

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		//SharedPreferences
		pref = getSharedPreferences("pref", MODE_PRIVATE);
		is_store = pref.getInt("is_store", 0);

		// Tab 초기화     
		
		viewPager = (ViewPager) findViewById(R.id.pager);
		actionBar = getActionBar();
		
		tabsPagerAdapter = new TabsPagerAdapter(getSupportFragmentManager());
		tabsPagerAdapter.setContext(this);
		tabsPagerAdapter.saveState();
		viewPager.setOffscreenPageLimit(4);
		viewPager.setAdapter(tabsPagerAdapter);
		
		// ActionBar 설정
		// 사장님페이지 ActionBar
		if(is_store==1) {
			actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_yellow_bg));
			actionBar.setStackedBackgroundDrawable(getResources().getDrawable(R.drawable.y_main_category_bg));
		}
		else {
			actionBar.setStackedBackgroundDrawable(getResources().getDrawable(R.drawable.main_category_bg));
		}
		actionBar.setHomeButtonEnabled(false); //<버튼 false
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setLogo(R.drawable.actionbar_logo);
		actionBar.setTitle(null);

		// Tab 추가		
		for (int tab_icon : tabsIcon_nor) {
			actionBar.addTab(actionBar.newTab().setIcon(tab_icon).setTabListener(this));
		}

		// ViewChangeListener
		viewPager.setOnPageChangeListener(simpleOnpageListener);

		// Button 설정
		joinButton = (Button)findViewById(R.id.btn_join);
		joinButton.setOnClickListener(this);
		if(is_store==1){
			joinButton.setVisibility(View.GONE);
		}
		else {
			joinButton.setVisibility(View.VISIBLE);
		}	
	}

	// ViewChangeListener : ViewPager -> Tab
	ViewPager.SimpleOnPageChangeListener simpleOnpageListener = new ViewPager.SimpleOnPageChangeListener() {
		@Override
		public void onPageSelected(int position) {
			actionBar.setSelectedNavigationItem(position);
			if (is_store==1) {
				actionBar.getTabAt(position).setIcon(tabsIcon_sel_y[position]);
				
			}
			else actionBar.getTabAt(position).setIcon(tabsIcon_sel[position]);
		}
	};

	// TabChangeListener : Tab -> ViewPager
	@Override
	public void onTabSelected(Tab tab, android.app.FragmentTransaction ft) {
		viewPager.setCurrentItem(tab.getPosition());
		switch (is_store) {
		case 0:
			actionBar.getTabAt(tab.getPosition()).setIcon(tabsIcon_sel[tab.getPosition()]);
			break;
		case 1:
			actionBar.getTabAt(tab.getPosition()).setIcon(tabsIcon_sel_y[tab.getPosition()]);
			break;
		}
	}

	@Override
	public void onTabUnselected(Tab tab, android.app.FragmentTransaction ft) {
		actionBar.getTabAt(tab.getPosition()).setIcon(tabsIcon_nor[tab.getPosition()]);
	}

	@Override
	public void onTabReselected(Tab tab, android.app.FragmentTransaction ft) {
	}

	// ActionBar & (res>menu>activity_actionbar.xml) 연결
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		if (is_store==1) inflater.inflate(R.menu.activity_actionbar_store, menu);
		else inflater.inflate(R.menu.activity_actionbar, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	// ActionBar Item onClick
	public boolean onOptionsItemSelected(MenuItem item) {

		Class actionBarClass = null;

		// 이동할 Class 지정
		switch (item.getItemId()) {

		case R.id.action_alarm:
//			if(is_store==1) actionBarClass = AlarmStorePage.class;
//			else actionBarClass = AlarmPage.class;
			actionBarClass = AlarmPage.class;
			break;
		case R.id.action_mypage:
			if(is_store==1) actionBarClass = MyPageActivity_Store.class;
			else actionBarClass = MyPageActivity.class;
			break;
		case R.id.action_more:
			actionBarClass = SettingPage.class;
			break;		
		}

		PageChange(actionBarClass);
		return super.onOptionsItemSelected(item);
	}

	// JoinButton onClick
	@Override
	public void onClick(View v) {
		PageChange(InfoRegistPage.class);
	}

	// ListItem onClick
	public void onClickListItem(View v) {
		//PageChange(CountListPage.class);
	}
	
	
	// Class 이동 intent
	void PageChange(Class goActionBarClass) {
		if (goActionBarClass != null) {
			Intent intent = new Intent(this, goActionBarClass);
			this.startActivity(intent);
		}
	}
	
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		super.onActivityResult(requestCode, resultCode, data);
//		
//		Log.d("MainActivity", "result : "+resultCode);
//		
//		switch (resultCode) {
//		   case 1001:
//			   Log.d("MainActivity", "main_inforeg");
//			   notiThread();
//		   break;
//
//		default:
//		   break;
//		}
//	}
//	
//	void notiThread() {
//		Handler handler = new Handler();
//		Runnable runnable = new Runnable() {
//			@Override
//			public void run() {
//				mNM = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//				
//				mNoti = new NotificationCompat.Builder(getBaseContext())
//						.setContentTitle("Accio")
//						.setContentText("Accio와 떨어졌습니다!!")
//						.setSmallIcon(R.drawable.ic_launcher)
//						.setTicker("Accio와 떨어졌습니다!!").build();
//				
//				mNM.notify(7777, mNoti);
//			}
//		};
//		
//		//Splash 시간 설정(ms)
//		handler.postDelayed(runnable, 3000);
//	}
}
