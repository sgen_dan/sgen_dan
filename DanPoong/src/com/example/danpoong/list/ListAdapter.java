package com.example.danpoong.list;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.danpoong.R;
import com.example.danpoong.actionbar_mypage.MyPageActivity;
import com.example.danpoong.countlist.CountListPage;
import com.example.danpoong.regist.RegisterPage;

public class ListAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private Context activity;
	private ArrayList<ListItem> array_data;
	TranslateAnimation animation;
	int lastposition = 0;

	SharedPreferences pref;
	int is_store;

	public String deleteItemId, deleteItemBoardType;
	
	public ListAdapter (Context mContext, ArrayList<ListItem> mArray) {
		this.activity=mContext;
		array_data=mArray;
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		//SharedPreferences
		pref = mContext.getSharedPreferences("pref", mContext.MODE_PRIVATE);
		is_store = pref.getInt("is_store", 0);
	}
	@Override
	public int getCount() {
		return array_data.size();
	}

	@Override
	public Object getItem(int position) {
		return array_data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView==null) {
			int resource=0;
			if(is_store==1) resource=R.layout.y_list_item;
			else resource=R.layout.list_item;
			convertView = inflater.inflate(resource, parent, false);
		}
		
		//xml 연결 - 사용자 정보
		TextView itemName=(TextView)convertView.findViewById(R.id.item_user_name);
		TextView itemUpdate=(TextView)convertView.findViewById(R.id.item_user_update);
		//xml 연결 - item 정보
		TextView itemCity=(TextView)convertView.findViewById(R.id.item_city);
		TextView itemLocation=(TextView)convertView.findViewById(R.id.item_location);
		TextView itemPrice=(TextView)convertView.findViewById(R.id.item_user_price);
		TextView itemPerson=(TextView)convertView.findViewById(R.id.item_user_person);
		TextView itemKind=(TextView)convertView.findViewById(R.id.item_user_kind);
		TextView itemDate=(TextView)convertView.findViewById(R.id.item_user_date);
		TextView itemMemo=(TextView)convertView.findViewById(R.id.item_user_memo);
		//xml 연결 - 입찰내역
		TextView itemCount=(TextView)convertView.findViewById(R.id.item_count);

		//data 넣기 - string data (array_data 연결)
		//사용자 정보
		String email = array_data.get(position).userName;
		if(email.length() > 7) {
			int pos = email.lastIndexOf( "@" );
			
			email = email.substring(0, pos);
		}
		
		itemName.setText(email);
		itemUpdate.setText(array_data.get(position).userUpdate);
		//item 정보
		itemCity.setText(array_data.get(position).userCity);
		itemLocation.setText(array_data.get(position).userLocation);
		itemPrice.setText(array_data.get(position).userPrice);
		itemPerson.setText(array_data.get(position).userPerson);
		itemKind.setText(array_data.get(position).userKind);
		itemDate.setText(array_data.get(position).userDate);
		itemMemo.setText(array_data.get(position).userMemo);
		//입찰내역
		itemCount.setText(array_data.get(position).userCount);
		//
		final String id = array_data.get(position).id;
		final String board_type = array_data.get(position).board_type;
		final String board_user = array_data.get(position).userName;
		
		itemCount.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(activity, CountListPage.class);
				intent.putExtra("id", id);
				intent.putExtra("board_type", board_type);
				intent.putExtra("board_user", board_user);
				activity.startActivity(intent);
			}

		});
		
		deleteItemId=array_data.get(position).id;
		deleteItemBoardType=array_data.get(position).board_type;
		
		/*if (position >= lastposition && (position != 1))
	        animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF,
	                0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
	                Animation.RELATIVE_TO_SELF, 0.5f,
	                Animation.RELATIVE_TO_SELF, 0.0f);
	    else {

	    	if(position != 0)
	        animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF,
	                0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
	                Animation.RELATIVE_TO_SELF, -0.5f,
	                Animation.RELATIVE_TO_SELF, 0.0f);
	    }*/

		if(!((position==1 && lastposition==0) || (position==0 && lastposition==0)|| (position==0 && lastposition==1))) {
			if (position >= lastposition )
				animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF,
						0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
						Animation.RELATIVE_TO_SELF, 0.5f,
						Animation.RELATIVE_TO_SELF, 0.0f);
			else {
				animation = new TranslateAnimation(Animation.RELATIVE_TO_SELF,
						0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
						Animation.RELATIVE_TO_SELF, -0.5f,
						Animation.RELATIVE_TO_SELF, 0.0f);
			}
			animation.setDuration(400);
			convertView.startAnimation(animation);
		}
		lastposition = position;
		return convertView;
	}
}
