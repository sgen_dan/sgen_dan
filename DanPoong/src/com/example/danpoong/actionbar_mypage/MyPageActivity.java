package com.example.danpoong.actionbar_mypage;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.text.Layout;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.danpoong.R;
import com.example.danpoong.countlist.CountListPage;

public class MyPageActivity  extends FragmentActivity implements ActionBar.TabListener{

	private ViewPager viewPager;
	private MyPageTabsPagerAdapter mypageTabsPagerAdapter;
	private ActionBar actionBar;

	// ActionBar title
	private String actionBarTitle = "마이페이지";

	// Tab Title
	private int[] tabsIcon_nor = {R.drawable.ic_home_nor, R.drawable.ic_bus_nor, R.drawable.ic_lunchbox_nor, R.drawable.ic_restaurant_nor};
	private int[] tabsIcon_sel = {R.drawable.ic_home_sel, R.drawable.ic_bus_sel, R.drawable.ic_lunchbox_sel, R.drawable.ic_restaurant_sel};

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mypage);

		// Tab 초기화
		viewPager = (ViewPager) findViewById(R.id.mypage_pager);
		mypageTabsPagerAdapter = new MyPageTabsPagerAdapter(getSupportFragmentManager());
		mypageTabsPagerAdapter.setContext(this);
		viewPager.setAdapter(mypageTabsPagerAdapter);

		//ActionBar 설정
		actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(true);
		actionBar.setLogo(R.drawable.nobackground);
		actionBar.setTitle(actionBarTitle);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setStackedBackgroundDrawable(getResources().getDrawable(R.drawable.main_category_bg));

		// Tab 추가		
		for (int tab_icon : tabsIcon_nor) {
			actionBar.addTab(actionBar.newTab().setIcon(tab_icon).setTabListener(this));
		}

		// ViewChangeListener
		viewPager.setOnPageChangeListener(simpleOnpageListener);
	}

	// ViewChangeListener : ViewPager -> Tab
	ViewPager.SimpleOnPageChangeListener simpleOnpageListener = new ViewPager.SimpleOnPageChangeListener() {
		@Override
		public void onPageSelected(int position) {
			actionBar.setSelectedNavigationItem(position);
			actionBar.getTabAt(position).setIcon(tabsIcon_sel[position]);
		}
	};

	// TabChangeListener : Tab -> ViewPager
	@Override
	public void onTabSelected(Tab tab, android.app.FragmentTransaction ft) {
		viewPager.setCurrentItem(tab.getPosition());
		actionBar.getTabAt(tab.getPosition()).setIcon(tabsIcon_sel[tab.getPosition()]);
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		actionBar.getTabAt(tab.getPosition()).setIcon(tabsIcon_nor[tab.getPosition()]);
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
	}

	//뒤로가기 버튼이 바로 전 단계로 가도록 설정
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	};

	//입찰내역 페이지 이동
	public void onClickListItem(View v) {
		PageChange(CountListPage.class);
	}	

	//Class 이동 intent
	void PageChange(Class goActionBarClass) {
		if (goActionBarClass != null) {
			Intent intent = new Intent(this, goActionBarClass);
			this.startActivity(intent);
		}
	}
}
