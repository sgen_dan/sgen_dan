package com.example.danpoong.actionbar_mypage;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.example.danpoong.R;
import com.example.danpoong.addstore.AddStore;
import com.example.danpoong.countlist.CountListAdapter;
import com.example.danpoong.countlist.CountListItem;
import com.example.danpoong.tab.HotelList.AsyncTaskParseJson;

public class MyPageActivity_Store extends Activity implements
SwipeRefreshLayout.OnRefreshListener {

	// SharedPreferences
	SharedPreferences pref;
	int is_store;

	//ListView
	private ArrayList<CountListItem> mArray;
	private CountListAdapter adapter;
	private Button storePlusButton;

	//Refresh
	SwipeRefreshLayout refreshLayout;
	String board_id, board_type, board_user;
	String store_name;

	ProgressDialog pDialog;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent get_info = getIntent();
		board_id = get_info.getStringExtra("id");
		board_type = get_info.getStringExtra("board_type");
		board_user = get_info.getStringExtra("board_user");

		// SharedPreferences
		pref = getSharedPreferences("pref", MODE_PRIVATE);
		is_store = pref.getInt("is_store", 0);
		store_name = pref.getString("store_name", "");

		// ActionBar 설정
		ActionBar actionBar = getActionBar();
		if (is_store == 1) {
			actionBar.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.actionbar_yellow_bg));
		}
		actionBar.setDisplayShowHomeEnabled(false); // 기본로고 OFF
		actionBar.setHomeButtonEnabled(true); //뒤로가기버튼ON

		// View 설정
		setContentView(R.layout.activity_countlist);
		setList();

		refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swype_countlist);
		refreshLayout.setOnRefreshListener(this);

		refreshLayout.setColorScheme(android.R.color.holo_blue_bright,
				android.R.color.holo_green_light,
				android.R.color.holo_orange_light,
				android.R.color.holo_red_light);


		if(is_store==1) refreshLayout.setColorScheme(R.color.picknic_yellow);
		else refreshLayout.setColorScheme(R.color.picknic_blue);


		// ★아래 초기화 부분이 현재 문제임!!
		// 2레벨 캐시(이미지 파일 캐시)를 사용하려면 동일 이름의 파일 캐시를 생성해 주어야 한다.
		// FileCacheFactory.getInstance().create("imagecache", 512);
		// 이미지 캐시 초기화
		// ImageCacheFactory.getInstance().createTwoLevelCache("imagecache",
		// 40);

		// Button 설정

		storePlusButton = (Button) findViewById(R.id.btn_store_plus);
		storePlusButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				PageChange(AddStore.class); // 입찰하기 등록 페이지(addstore)로 이동

			}
		});

		if (is_store == 1) {
			storePlusButton.setBackgroundResource(R.drawable.y_main_btn_plus);
			storePlusButton.setVisibility(View.VISIBLE);
		} else
			storePlusButton.setVisibility(View.GONE);

		new AsyncTaskParseJson().execute();
	}

	// ActionBar & (res>menu>activity_actionbar.xml) 연결
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	//뒤로가기 버튼이 바로 전 단계로 가도록 설정
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.action_settings:
			return true;
		}
		return super.onOptionsItemSelected(item);
	};

	@Override
	public void onRefresh() {
		// refresh
		//		new Handler().postDelayed(new Runnable() {
		//			@Override
		//			public void run() {
		//				refreshLayout.setRefreshing(false);
		//			}
		//		}, 3000);
		new AsyncTaskParseJson().execute();
	}

	// Item 추가 - DB연동 필요
	private void addItem() {
		mArray = new ArrayList<CountListItem>();

		String[] hihi = {
				"http://movie.phinf.naver.net/20141124_107/141679124450580TTS_JPEG/movie_image.jpg",
				"http://movie.phinf.naver.net/20141103_255/1414988497460e1QB6_JPEG/movie_image.jpg",
		"http://movie.phinf.naver.net/20141120_253/1416449434640NRPvz_JPEG/movie_image.jpg" };
		String[] hihi2 = {
				"http://movie.phinf.naver.net/20141219_160/1418963692905gWFQm_JPEG/movie_image.jpg",
		"http://movie.phinf.naver.net/20150105_272/1420423209179jYaLt_JPEG/movie_image.jpg" };

		mArray.add(new CountListItem("서가낙지", "2014/12/27(토)", "01042008968",
				"인천","중식", "7,000", "맛있고 푸짐합니다.\n정성스러운 음식으로 대접하겠습니다.", 3, hihi));
		mArray.add(new CountListItem("오목골", "2014/12/27(토)", "01031720575",
				"인천","양식", "10,000", "맛있고 푸짐합니다.\n정성스러운 음식으로 대접하겠습니다.", 3, hihi2));
	}

	// List 생성
	private void setList() {
		ListView listView = (ListView) findViewById(R.id.countlist_listview);
		//addItem();
		mArray = new ArrayList<CountListItem>();
		adapter = new CountListAdapter(MyPageActivity_Store.this, mArray);
		adapter.notifyDataSetChanged();
		listView.setAdapter(adapter);
	}

	// Class 이동 intent
	void PageChange(Class goActionBarClass) {
		if (goActionBarClass != null) {
			Intent intent = new Intent(this, goActionBarClass);
			intent.putExtra("board_id", board_id);
			intent.putExtra("board_type", board_type);
			intent.putExtra("board_user", board_user);
			this.startActivity(intent);
		}
	}

	// //////////////////////////////////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////////////////////////////////

	public class AsyncTaskParseJson extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			refreshLayout.setRefreshing(true);
			pDialog = new ProgressDialog(MyPageActivity_Store.this);
			pDialog.setMessage("데이터 가져오는 중...");
			//pDialog.setIndeterminate(false); pDialog.setCancelable(true);
			pDialog.show();

		}

		@Override
		protected String doInBackground(String... arg0) {

			StringBuilder sb = new StringBuilder();

			try {

				// http://s940217.mireene.com/get_register_hotel.php?location="전체"&type="전체"
				String surl = "http://s940217.mireene.com/get_store_mypage.php?store_name=";
				surl = surl
						+ java.net.URLEncoder.encode(new String(store_name
								.getBytes("UTF-8")));

				Log.d("countlistpage", surl);
				URL url = new URL(surl);
				// URL url = new
				// URL("http://mapnoti.zz.mu/getdata.php?lat=37.5745215&lng=127.0388250&range=1.5");
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				if (conn != null) {

					conn.setConnectTimeout(5000);
					// conn.setUseCaches(false);
					System.out.println("connect");
					System.out.println(String.valueOf(conn.getResponseCode())
							+ "\n" + String.valueOf(HttpURLConnection.HTTP_OK));

					if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
						BufferedReader br = new BufferedReader(
								new InputStreamReader(conn.getInputStream()));

						while (true) {
							String line = br.readLine();
							if (line == null)
								break;
							sb.append(line + "\n");
						}
						br.close();

					} else {

					}
					conn.disconnect();
				}
			} catch (Exception e) {
				// Log.d("hotel", "connection error: "+e.toString());
				// Toast.makeText(this, e.toString(),
				// Toast.LENGTH_SHORT).show();
			}

			String jsonString = sb.toString();
			Log.d("countlistpage", "jsonString " + jsonString);
			if (jsonString.length() > 5) {
				try {
					if (!mArray.isEmpty())
						mArray.clear();

					JSONArray ja = new JSONArray(jsonString);
					Log.d("countlistpage", "ja.length: " + ja.length());

					for (int i = 0; i < ja.length(); i++) {
						JSONObject jo = ja.getJSONObject(i);

						String store_name = jo.getString("store_name");
						String now_date = jo.getString("now_date");// @drawable/img_nature1
						String telephone = jo.getString("telephone");
						String map = jo.getString("map");
						String type = jo.getString("type");
						String budget = jo.getString("budget");
						String content = jo.getString("content");
						int numberofpicture = jo.getInt("numberofpicture");
						String img_name1 = jo.getString("img_name1");
						String img_name2 = jo.getString("img_name2");
						String img_name3 = jo.getString("img_name3");

						String m_url = "http://s940217.mireene.com/image/uploads/";
						String[] img_url = {
								m_url + img_name1,
								m_url + img_name2,
								m_url + img_name3};

						if(numberofpicture == 1)
							numberofpicture = numberofpicture + 1;
						String[] img_array = new String[numberofpicture];
						for(int im=0; im<numberofpicture; im++)
							img_array[im] = img_url[im];

						if(img_array.length < 2) {
							//img_array[0] = "http://s940217.mireene.com/image/uploads/no_image.png";
							img_array[1] = "http://s940217.mireene.com/image/uploads/no_image.png";
						}
						Log.d("countlistpage", img_array[0]);

						mArray.add(new CountListItem(store_name, now_date, telephone,
								map, type, budget, content, numberofpicture, img_array));

					}
					// Log.d("hotel", ""+mArray.size());

				} catch (JSONException e) {
					// Toast.makeText(mContext, e.toString(),
					// Toast.LENGTH_SHORT).show();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(String strFromDoInBg) {
			pDialog.dismiss();
			refreshLayout.setRefreshing(false); // Refresh 애니메이션 Stop
			Log.d("countlistpage", "onPostExecute");
			Log.d("countlistpage", "" + mArray.size());
			adapter.notifyDataSetChanged();
		}

	}

}
