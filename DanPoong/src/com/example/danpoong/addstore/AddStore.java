package com.example.danpoong.addstore;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.danpoong.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class AddStore extends Activity implements OnClickListener {

	private Button bid_imageButton, uploadbutton;
	ImageProcess bid_imgPrcs = new ImageProcess();
	//
	ProgressDialog prgDialog;
	String encodedString;
	RequestParams params = new RequestParams();
	String imgPath;
	String fileName;
	Bitmap bitmap;
	private static int RESULT_LOAD_IMG = 1;
	TextView select_tv;
	//
	int numtoupload = 0;
	int num_uploaded = 0;
	String[] all_path;
	String board_id, board_type, board_user;
	//
	HttpPost httppost;
	StringBuffer buffer;
	HttpResponse response;
	HttpClient httpclient;
	List<NameValuePair> nameValuePairs;
	Spinner spin;
	Button signup_button;
	//
	EditText tv_budget, tv_content;
	//
	String[] img_name;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_addstore);

		select_tv = (TextView) findViewById(R.id.addstore_select_textview);
		select_tv.setText("사진이 0장 선택되었습니다.");
		// Action Bar 설정
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.actionbar_yellow_bg));
		actionBar.setDisplayShowHomeEnabled(false); // 기본로고 OFF

		// Button 설정
		bid_imageButton = (Button) findViewById(R.id.bid_loadImg);
		bid_imageButton.setOnClickListener(this);

		uploadbutton = (Button) findViewById(R.id.bid_done);
		uploadbutton.setOnClickListener(this);

		Intent get_info = getIntent();
		board_id = get_info.getStringExtra("board_id");
		board_type = get_info.getStringExtra("board_type");
		board_user = get_info.getStringExtra("board_user");
		//
		tv_budget = (EditText) findViewById(R.id.bid_editText1);
		tv_content = (EditText) findViewById(R.id.bid_editText2);

		img_name = new String[3];
		img_name[0] = "";
		img_name[1] = "";
		img_name[2] = "";
	}

	// ActionBar & (res>menu>activity_registbar.xml) 연결
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.bid_done) {
			num_uploaded = 0;
			uploadImage();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bid_loadImg:

			loadImagefromGallery(v);
			break;

		case R.id.bid_done:
			if (all_path.length > 3) {
				Toast.makeText(this, "사진은 최대 3장까지 첨부할 수 있습니다.",
						Toast.LENGTH_SHORT).show();
			} else {
				if(numtoupload == 0) {
					Toast.makeText(this, "선택된 사진이 없습니다.",
							Toast.LENGTH_SHORT).show();
				}
				else {
					num_uploaded = 0;
					uploadImage();
				}
			}
			break;
		}
	}

	// //////////////////////////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////////////////////////

	public void loadImagefromGallery(View view) {
		// Create intent to Open Image applications like Gallery, Google Photos
		/*
		 * Intent galleryIntent = new Intent(Action.ACTION_MULTIPLE_PICK,
		 * android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		 */
		Intent galleryIntent = new Intent(Action.ACTION_MULTIPLE_PICK);
		// Start the Intent
		startActivityForResult(galleryIntent, 200);
	}

	// When Image is selected from Gallery
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		try {
			// When an Image is picked
			if (requestCode == 200 && resultCode == Activity.RESULT_OK
					&& null != data) {
				// Get the Image from data

				all_path = data.getStringArrayExtra("all_path");
				numtoupload = all_path.length;
				select_tv.setText("사진이 " + all_path.length + "장 선택되었습니다.");

				if (all_path.length > 3) {
					Toast.makeText(this, "사진은 최대 3장까지 첨부할 수 있습니다.",
							Toast.LENGTH_SHORT).show();
				} else {

					imgPath = all_path[0];
					String fileNameSegments[] = imgPath.split("/");
					fileName = fileNameSegments[fileNameSegments.length - 1];

					// int pos = fileName.lastIndexOf( "." );
					// fileName = fileName.substring(pos);

					// Put file name in Async Http Post Param which will used in
					// Php
					// web app

				}
				/*
				 * ArrayList<CustomGallery> dataT = new
				 * ArrayList<CustomGallery>();
				 * 
				 * for (String string : all_path) { CustomGallery item = new
				 * CustomGallery(); item.sdcardPath = string;
				 * 
				 * dataT.add(item); }
				 * 
				 * Uri selectedImage = data.getData(); String[] filePathColumn =
				 * { MediaStore.Images.Media.DATA };
				 * 
				 * // Get the cursor Cursor cursor =
				 * getContentResolver().query(selectedImage, filePathColumn,
				 * null, null, null); // Move to first row cursor.moveToFirst();
				 * 
				 * int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				 * imgPath = cursor.getString(columnIndex); cursor.close();
				 * //ImageView imgView = (ImageView) findViewById(R.id.imgView);
				 * // Set the Image in ImageView
				 * bid_imgView.setImageBitmap(BitmapFactory
				 * .decodeFile(imgPath)); // Get the Image's file name String
				 * fileNameSegments[] = imgPath.split("/"); fileName =
				 * fileNameSegments[fileNameSegments.length - 1]; // Put file
				 * name in Async Http Post Param which will used in Php // web
				 * app params.put("filename", fileName);
				 */

			} else {
				Toast.makeText(this, "You haven't picked Image",
						Toast.LENGTH_SHORT).show();
			}
		} catch (Exception e) {
			Toast.makeText(this, "다시 시도해주시기 바랍니다.", Toast.LENGTH_SHORT).show();
		}

	}

	// When Upload button is clicked
	public void uploadImage() {
		// When Image is selected from Gallery
		if (all_path != null && !all_path[0].isEmpty()) {
			prgDialog = new ProgressDialog(AddStore.this);
			prgDialog.setMessage("Converting Image to Binary Data");
			prgDialog.setCancelable(false);
			prgDialog.show();
			// Convert image to String using Base64
			encodeImagetoString();
			// When Image is not selected from Gallery
		} else {
			Toast.makeText(
					AddStore.this,
					"You must select image from gallery before you try to upload",
					Toast.LENGTH_LONG).show();
		}

	}

	// AsyncTask - To convert Image to String
	public void encodeImagetoString() {
		new AsyncTask<Void, Void, String>() {

			protected void onPreExecute() {

			};

			@Override
			protected String doInBackground(Void... params) {
				BitmapFactory.Options options = null;
				options = new BitmapFactory.Options();
				options.inSampleSize = 4;
				bitmap = BitmapFactory.decodeFile(all_path[num_uploaded],
						options);
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				// Must compress the Image to reduce image size to make upload
				// easy
				bitmap.compress(Bitmap.CompressFormat.PNG, 50, stream);
				byte[] byte_arr = stream.toByteArray();
				// Encode Image to String
				encodedString = Base64.encodeToString(byte_arr, 0);
				return "";
			}

			@Override
			protected void onPostExecute(String msg) {
				prgDialog.setMessage("Calling Upload");
				// Put converted Image string into Async Http Post param
				SharedPreferences pref = getSharedPreferences("pref",
						MODE_PRIVATE);
				String store_name = pref.getString("store_name", "");
				String img_name_server = num_uploaded + "_" + board_id + "_"
						+ board_type + "_" + fileName;
				img_name[num_uploaded] = img_name_server;
				params.put("filename", img_name_server);
				params.put("image", encodedString);
				// Trigger Image upload
				triggerImageUpload();
			}
		}.execute(null, null, null);
	}

	public void triggerImageUpload() {
		makeHTTPCall();
	}

	// http://192.168.2.4:9000/imgupload/upload_image.php
	// Make Http call to upload Image to Php server
	public void makeHTTPCall() {
		prgDialog.setMessage((num_uploaded + 1) + "번째 사진 업로드 중...");
		AsyncHttpClient client = new AsyncHttpClient();
		// Don't forget to change the IP address to your LAN address. Port no as
		// well.
		client.setTimeout(30000);
		client.post("http://s940217.mireene.com/image/uploadimage.php", params,
				new AsyncHttpResponseHandler() {
					// When the response returned by REST has Http
					// response code '200'
					@Override
					public void onSuccess(String response) {
						// Hide Progress Dialog
						// prgDialog.hide();
						Toast.makeText(getApplicationContext(), response,
								Toast.LENGTH_SHORT).show();
						num_uploaded++;
						// //////////////////////////////////////////////////////////////////////////
						if (num_uploaded < numtoupload)
							uploadImage();
						else {

							prgDialog.setMessage("db 저장중...");
							new Thread(new Runnable() {
								public void run() {
									login();
								}
							}).start();
						}
					}

					// When the response returned by REST has Http
					// response code other than '200' such as '404',
					// '500' or '403' etc
					@Override
					public void onFailure(int statusCode, Throwable error,
							String content) {
						// Hide Progress Dialog
						prgDialog.hide();
						// When Http response code is '404'
						if (statusCode == 404) {
							Toast.makeText(getApplicationContext(),
									"Requested resource not found",
									Toast.LENGTH_LONG).show();
						}
						// When Http response code is '500'
						else if (statusCode == 500) {
							Toast.makeText(getApplicationContext(),
									"Something went wrong at server end",
									Toast.LENGTH_LONG).show();
						}
						// When Http response code other than 404, 500
						else {
							Toast.makeText(
									getApplicationContext(),
									"Error Occured \n Most Common Error: \n1. Device not connected to Internet\n2. Web App is not deployed in App server\n3. App server is not running\n HTTP Status code : "
											+ statusCode, Toast.LENGTH_LONG)
									.show();
						}
					}
				});
	}

	// /////////////////////////////////////////////////////////////////////////////////////////
	void login() {
		try {

			httpclient = new DefaultHttpClient();
			String surl = "http://s940217.mireene.com/bid_register.php";

			httppost = new HttpPost(surl);

			// httppost.setEntity();
			// add your data
			nameValuePairs = new ArrayList<NameValuePair>(8);// 8+1
			// httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
			// HTTP.));

			// Always use the same variable name for posting i.e the android
			// side variable name and php side variable name should be similar,

			nameValuePairs.add(new BasicNameValuePair("board_id", board_id));

			nameValuePairs
					.add(new BasicNameValuePair("board_type", board_type));
			
			nameValuePairs
			.add(new BasicNameValuePair("board_user", board_user));

			SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);

			nameValuePairs.add(new BasicNameValuePair("store_name", pref
					.getString("store_name", "")));

			nameValuePairs.add(new BasicNameValuePair("telephone", pref
					.getString("store_tel", "")));

			nameValuePairs.add(new BasicNameValuePair("type", pref.getString(
					"store_type", "")));

			nameValuePairs.add(new BasicNameValuePair("budget", tv_budget
					.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("content", tv_content
					.getText().toString()));

			nameValuePairs.add(new BasicNameValuePair("numberofpicture", String
					.valueOf(numtoupload)));
			//
			nameValuePairs
					.add(new BasicNameValuePair("img_name1", img_name[0]));
			nameValuePairs
					.add(new BasicNameValuePair("img_name2", img_name[1]));
			nameValuePairs
					.add(new BasicNameValuePair("img_name3", img_name[2]));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
					HTTP.UTF_8));
			// Execute HTTP Post Request
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			final String response = httpclient.execute(httppost,
					responseHandler);

			// ResponseHandler<String> responseHandler = new
			// BasicResponseHandler();
			// final String response = httpclient.execute(httppost,
			// responseHandler);
			System.out.println("Response : " + response);
			runOnUiThread(new Runnable() {
				public void run() {
					// tv.setText("Response from PHP : " + response);
					System.out.println(response);
					prgDialog.dismiss();
				}
			});

			if (response.equalsIgnoreCase("Sign Up")) {
				runOnUiThread(new Runnable() {
					public void run() {
						Toast.makeText(AddStore.this, "Upload Success",
								Toast.LENGTH_SHORT).show();
						finish();
					}
				});

			} else {
				showAlert();
			}

		} catch (Exception e) {
			prgDialog.dismiss();
			System.out.println("Exception : " + e.getMessage());
		}
	}

	public void showAlert() {
		AddStore.this.runOnUiThread(new Runnable() {
			public void run() {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						AddStore.this,
						ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);
				builder.setTitle("Login Error.");
				builder.setMessage("다시 시도해 주시기 바랍니다.")
						.setCancelable(false)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
			}
		});
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// Dismiss the progress bar when application is closed
		if (prgDialog != null) {
			prgDialog.dismiss();
		}
	}
}
