package com.example.danpoong;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.danpoong.login.LoginPage;

public class SplashPage extends Activity{

	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		Handler handler = new Handler();
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				Intent intent = new Intent(SplashPage.this , LoginPage.class);
				startActivity(intent);
				SplashPage.this.finish();
			}
		};
		
		//Splash �ð� ����(ms)
		handler.postDelayed(runnable, 3000);
	}
}