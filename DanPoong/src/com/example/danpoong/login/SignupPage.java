package com.example.danpoong.login;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.example.danpoong.MainActivity;
import com.example.danpoong.QuestionPage;
import com.example.danpoong.R;
import com.example.danpoong.R.id;
import com.example.danpoong.R.layout;
import com.example.danpoong.R.menu;
import com.example.danpoong.regist.RegisterPage;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class SignupPage extends Activity implements OnItemSelectedListener {
	
	private String[] SignupItemsKind = { "gmail.com", "naver.com", "daum.net",
			"nate.com", "yahoo.com" }; // 이메일 종류
	private ActionBar actionBar;
	//
	EditText email, password1, password2;
	HttpPost httppost;
	StringBuffer buffer;
	HttpResponse response;
	HttpClient httpclient;
	List<NameValuePair> nameValuePairs;
	ProgressDialog dialog;
	Spinner spin;
	boolean isStore;
	Button signup_button;

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
				
		Intent intentfromto = getIntent();
		isStore = intentfromto.getBooleanExtra("isStoreMode", false);
		
		if(isStore==true) setContentView(R.layout.activity_signup_store);
		else setContentView(R.layout.activity_signup);

		// ActionBar
		actionBar = getActionBar();
		if(isStore==true) {
			actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar_yellow_bg));
		}
		actionBar.setDisplayShowHomeEnabled(false);
		//
		email = (EditText) findViewById(R.id.signup_edittext1);
		password1 = (EditText) findViewById(R.id.signup_edittext2);
		password2 = (EditText) findViewById(R.id.signup_edittext3);
		signup_button = (Button) findViewById(R.id.signup_button1);
		signup_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (password1.getText().toString().length() < 8)
					showAlert_message("비밀번호는 최소 8자 이상이어야 합니다.");
				else if (!password1.getText().toString()
						.equals(password2.getText().toString()))
					showAlert_message("비밀번호가 일치하지 않습니다.");
				else {

					dialog = new ProgressDialog(SignupPage.this,
							ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);
					dialog.setMessage("회원가입 중...");
					// pDialog.setIndeterminate(false);
					dialog.setCancelable(true);
					dialog.show();

					new Thread(new Runnable() {
						public void run() {
							login();
						}
					}).start();
				}
			}
		});
		// Spinner
		spin = (Spinner) findViewById(R.id.signup_spinner);
		spin.setOnItemSelectedListener(this); // 액티비티 스스로를 선택 변경 이벤트에 대한 리스너로 등록
		ArrayAdapter<String> aa = new ArrayAdapter<String>(this,
				R.layout.signup_style, SignupItemsKind);
		// 드롭다운 화면에 표시
		aa.setDropDownViewResource(R.layout.signup_category_style);
		spin.setAdapter(aa);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	void login() {
		try {

			httpclient = new DefaultHttpClient();
			httppost = new HttpPost("http://s940217.mireene.com/signup.php");

			String email_address = email.getText().toString().trim() + "@"
					+ spin.getSelectedItem().toString();
			// add your data
			nameValuePairs = new ArrayList<NameValuePair>(3);
			// Always use the same variable name for posting i.e the android
			// side variable name and php side variable name should be similar,
			nameValuePairs
					.add(new BasicNameValuePair("username", email_address)); // $Edittext_value
			// =
			// $_POST['Edittext_value'];
			nameValuePairs.add(new BasicNameValuePair("password", password1
					.getText().toString().trim()));
			
			nameValuePairs.add(new BasicNameValuePair("is_store", String.valueOf((isStore)? 1:0)));
			
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			// Execute HTTP Post Request
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			final String response = httpclient.execute(httppost,
					responseHandler);

			// ResponseHandler<String> responseHandler = new
			// BasicResponseHandler();
			// final String response = httpclient.execute(httppost,
			// responseHandler);
			System.out.println("Response : " + response);
			runOnUiThread(new Runnable() {
				public void run() {
					// tv.setText("Response from PHP : " + response);
					System.out.println(response);
					dialog.dismiss();
				}
			});

			if (response.equalsIgnoreCase("Sign Up")) {
				runOnUiThread(new Runnable() {
					public void run() {
						Toast.makeText(SignupPage.this, "Sign up Success",
								Toast.LENGTH_SHORT).show();
						if (!isStore)
							finish();
						else {
							Intent intent = new Intent(SignupPage.this,
									RegisterPage.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							String email_address = email.getText().toString().trim() + "@"
									+ spin.getSelectedItem().toString();
							intent.putExtra("email_address", email_address);
							startActivity(intent);
							finish();
						}
					}
				});

			} else {
				showAlert();
			}

		} catch (Exception e) {
			dialog.dismiss();
			System.out.println("Exception : " + e.getMessage());
		}
	}

	public void showAlert() {
		SignupPage.this.runOnUiThread(new Runnable() {
			public void run() {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						SignupPage.this,
						ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);
				builder.setTitle("Login Error.");
				builder.setMessage("이미 가입한 이메일입니다.")
						.setCancelable(false)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
			}
		});
	}

	public void showAlert_message(final String mes) {
		SignupPage.this.runOnUiThread(new Runnable() {
			public void run() {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						SignupPage.this,
						ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);
				builder.setTitle("Login Error.");
				builder.setMessage(mes)
						.setCancelable(false)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
			}
		});
	}

}
