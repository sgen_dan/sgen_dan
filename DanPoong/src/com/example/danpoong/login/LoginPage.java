package com.example.danpoong.login;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.danpoong.MainActivity;
import com.example.danpoong.QuestionPage;
import com.example.danpoong.R;

public class LoginPage extends Activity {

	Button Login_Button;
	Button Nomember_Button;
	String str = "아직 회원이 아니세요?";
	
	EditText et, pass;
	HttpPost httppost;
	StringBuffer buffer;
	HttpResponse response;
	HttpClient httpclient;
	List<NameValuePair> nameValuePairs;
	ProgressDialog dialog;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		Login_Button = (Button) findViewById(R.id.login_button1);
		Nomember_Button = (Button) findViewById(R.id.login_button2);
		
		TextView t = (TextView)findViewById(R.id.login_button2);
		t.setText(Html.fromHtml("<u>"+ str + "</u>"));	// "아직 회원이 아니세요?"에 밑줄
		
		//
		et = (EditText) findViewById(R.id.login_editText1);
		pass = (EditText) findViewById(R.id.login_editText2);

		Login_Button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				/*
				 * Intent loginIntent = new Intent(LoginPage.this,
				 * MainActivity.class); startActivity(loginIntent);
				 */
				dialog = new ProgressDialog(LoginPage.this, ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);
				dialog.setMessage("Validating user...");
	            //pDialog.setIndeterminate(false);
				dialog.setCancelable(true);
				dialog.show();
				
				new Thread(new Runnable() {
					public void run() {
						login();
					}
				}).start();

			}
		});

		Nomember_Button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent SignupIntent = new Intent(LoginPage.this,
						QuestionPage.class);
				startActivity(SignupIntent);
			}
		});

	}

	void login() {
		try {

			httpclient = new DefaultHttpClient();
			httppost = new HttpPost("http://s940217.mireene.com/login_check.php");
			nameValuePairs = new ArrayList<NameValuePair>(2);
			// Always use the same variable name for posting i.e the android
			// side variable name and php side variable name should be similar,
			nameValuePairs.add(new BasicNameValuePair("username",et.getText()
					.toString().trim())); // $Edittext_value
																		// =
																		// $_POST['Edittext_value'];
			nameValuePairs.add(new BasicNameValuePair("password",pass.getText()
					.toString().trim()));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			// Execute HTTP Post Request
			
			//response = httpclient.execute(httppost);
			
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			final String response = httpclient.execute(httppost, responseHandler);
			
			System.out.println("Response : " + response);
			runOnUiThread(new Runnable() {
				public void run() {
					// tv.setText("Response from PHP : " + response);
					System.out.println(response);
					dialog.dismiss();
				}
			});
			//
			if (response.equalsIgnoreCase("User Found")) {
				runOnUiThread(new Runnable() {
					public void run() {
						Toast.makeText(LoginPage.this,
								"Login Success", Toast.LENGTH_SHORT).show();
						new AsyncTaskParseJson().execute();
					}
				});
				
				//SharedPreferences
				/*SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
		        SharedPreferences.Editor editor = pref.edit();
		        editor.putInt("is_store", 0);
		        editor.commit();*/
				
				/*startActivity(new Intent(LoginPage.this,
						MainActivity.class));*/
			} else {
				showAlert();
			}

		} catch (Exception e) {
			dialog.dismiss();
			System.out.println("Exception : " + e.getMessage());
		}
	}
	
	public void showAlert(){
		LoginPage.this.runOnUiThread(new Runnable() {
		    public void run() {
		    	AlertDialog.Builder builder = new AlertDialog.Builder(LoginPage.this, ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);
		    	builder.setTitle("Login Error.");
		    	builder.setMessage("아이디 또는 비밀번호를 다시 확인하세요.")  
		    	       .setCancelable(false)
		    	       .setPositiveButton("OK", new DialogInterface.OnClickListener() {
		    	           public void onClick(DialogInterface dialog, int id) {
		    	           }
		    	       });	       
		    	AlertDialog alert = builder.create();
		    	alert.show();		    	
		    }
		});
	}
	
	///////////////////////////////////////////////////////////////////////////////
	public class AsyncTaskParseJson extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(LoginPage.this, ProgressDialog.THEME_DEVICE_DEFAULT_LIGHT);
			dialog.setMessage("사용자 정보 가져오는 중...");
            //pDialog.setIndeterminate(false);
			dialog.setCancelable(true);
			dialog.show();
			/*
			 * pDialog = new ProgressDialog(mContext);
			 * pDialog.setMessage("데이터 가져오는 중...");
			 * //pDialog.setIndeterminate(false); pDialog.setCancelable(true);
			 * pDialog.show();
			 */
		}

		@Override
		protected String doInBackground(String... arg0) {

			StringBuilder sb = new StringBuilder();

			try {

				// http://s940217.mireene.com/get_register_hotel.php?location="전체"&type="전체"
				String surl = "http://s940217.mireene.com/get_login_info.php?username=";
				surl = surl
						+ java.net.URLEncoder.encode(new String(et.getText()
								.toString().trim()
								.getBytes("UTF-8")));
				
				URL url = new URL(surl);
				// URL url = new
				// URL("http://mapnoti.zz.mu/getdata.php?lat=37.5745215&lng=127.0388250&range=1.5");
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				if (conn != null) {

					conn.setConnectTimeout(10000);
					// conn.setUseCaches(false);
					System.out.println("connect");
					System.out.println(String.valueOf(conn.getResponseCode())
							+ "\n" + String.valueOf(HttpURLConnection.HTTP_OK));

					if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
						BufferedReader br = new BufferedReader(
								new InputStreamReader(conn.getInputStream()));

						while (true) {
							String line = br.readLine();
							if (line == null)
								break;
							sb.append(line + "\n");
						}
						br.close();

					} else {

					}
					conn.disconnect();
				}
			} catch (Exception e) {
				// Log.d("hotel", "connection error: "+e.toString());
				Toast.makeText(LoginPage.this, e.toString(), Toast.LENGTH_SHORT)
				.show();
			}

			String jsonString = sb.toString();
			Log.d("User Info", "jsonString " + jsonString);
			if(jsonString.length() > 5) {
				try {
					
					JSONArray ja = new JSONArray(jsonString);
					//Log.d("hotel", "ja.length: " + ja.length());

					for (int i = 0; i < ja.length(); i++) {
						JSONObject jo = ja.getJSONObject(i);
						
						int id = jo.getInt("id");
						String username = jo.getString("username");// @drawable/img_nature1
						int is_store = jo.getInt("is_store");
						//
						String store_name = jo.getString("store_name");
						String representative = jo.getString("representative");
						String store_tel = jo.getString("store_tel");
						String store_address = jo.getString("store_address");
						String store_type = jo.getString("store_type");
						String registration_number = jo.getString("registration_number");
						String business_hours = jo.getString("business_hours");
						String content = jo.getString("content");
						
						//SharedPreferences
						SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
				        SharedPreferences.Editor editor = pref.edit();
				        
				        
				        editor.putInt("id", id);
				        editor.putString("username", username);
				        editor.putInt("is_store", is_store);
				        //
				        editor.putString("store_name", store_name);
				        editor.putString("representative", representative);
				        editor.putString("store_tel", store_tel);
				        editor.putString("store_type", store_type);
				        editor.putString("store_address", store_address);
				        editor.putString("registration_number", registration_number);
				        editor.putString("business_hours", business_hours);
				        editor.putString("content", content);
				        
				        editor.commit();

					}
					// Log.d("hotel", ""+mArray.size());

				} catch (JSONException e) {
					// Log.d("hotel", "Json Array Error: "+e.toString());
					Toast.makeText(LoginPage.this, e.toString(), Toast.LENGTH_SHORT)
					.show();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(String strFromDoInBg) {
			// pDialog.dismiss();
			dialog.dismiss();
			startActivity(new Intent(LoginPage.this,
					MainActivity.class));
			Log.d("hotel", "onPostExecute");
		}

	}
	
}
