package com.example.danpoong.tab;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

import com.example.danpoong.R;
import com.example.danpoong.list.ListAdapter;
import com.example.danpoong.list.ListItem;
import com.example.danpoong.tab.PackedLunchTab.AsyncTaskParseJson;

public class RestaurantTab extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

	//Flag
	private boolean isMain;

	//Fragment
	private Context mContext;
	private View rootView;

	//ListView
	private ArrayList<ListItem> mArray;
	private ListAdapter adapter;
	private ListView listView;

	//Refresh
	private SwipeRefreshLayout refreshLayout;

	//Spinner String
	private String[] restaurantItemLocation ={"전체","서울","경기","인천","부산","대구","광주","대전","울산","강원","경남","경북","전남","전북","충남","충북","제주"};
	private String[] restaurantItemsKind ={"전체","한식","양식","중식","고기","술집","기타"};
	Spinner restaurantSpinnerLocation;
	Spinner restaurantSpinnerKind;

	//Delete 하기 위해 넘겨줄 변수들
	private String selectedItemId, selectedItemBoardType; 

	public RestaurantTab (Context mContext, boolean isMain) {
		this.mContext = mContext;
		this.isMain = isMain;
	}

	public View onCreateView (LayoutInflater inflager, ViewGroup container, Bundle savedInstanceState) {

		//SharedPreferences
		SharedPreferences pref = mContext.getSharedPreferences("pref", mContext.MODE_PRIVATE);
		int is_store = pref.getInt("is_store", 0);

		rootView = inflager.inflate(R.layout.fragment_restaurant, container, false);
		RestaurantList(mContext, rootView);

		/*Spinner - Location*/
		restaurantSpinnerLocation = (Spinner)rootView.findViewById(R.id.restaurant_spinner_location);
		//Store, User 구분
		if(is_store==1) restaurantSpinnerLocation.setBackgroundResource(R.drawable.y_main_dropdown_menu_1);
		else restaurantSpinnerLocation.setBackgroundResource(R.drawable.main_dropdown_menu);
		//Adapter 연결
		ArrayAdapter<String> restaurantAdapterLocation = new ArrayAdapter<String>(mContext, R.layout.spinner_item, restaurantItemLocation);
		restaurantAdapterLocation.setDropDownViewResource(R.layout.spinner_item_dropdown);
		restaurantSpinnerLocation.setAdapter(restaurantAdapterLocation);
		//Listener 연결
		restaurantSpinnerLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				onRefresh();
			}
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		/*Spinner - Kind*/
		restaurantSpinnerKind = (Spinner)rootView.findViewById(R.id.restaurant_spinner_kind);
		//Store, User 구분
		if(is_store==1) restaurantSpinnerKind.setBackgroundResource(R.drawable.y_main_dropdown_menu_1);
		else restaurantSpinnerKind.setBackgroundResource(R.drawable.main_dropdown_menu);
		//Adapter 연결
		ArrayAdapter<String> restaurantAdapterKind = new ArrayAdapter<String>(mContext, R.layout.spinner_item, restaurantItemsKind);
		restaurantAdapterKind.setDropDownViewResource(R.layout.spinner_item_dropdown);
		restaurantSpinnerKind.setAdapter(restaurantAdapterKind);
		//Listener 연결
		restaurantSpinnerKind.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				onRefresh();
			}
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		//Refresh 설정
		refreshLayout = (SwipeRefreshLayout)rootView.findViewById(R.id.swipe_restaurantpage);
		if(is_store==1) refreshLayout.setColorScheme(R.color.picknic_yellow);
		else refreshLayout.setColorScheme(R.color.picknic_blue);
		refreshLayout.setOnRefreshListener(this);

		return rootView;
	}

	public void RestaurantList(Context mContext, View rootView) {
		this.mContext = mContext;
		this.rootView = rootView;

		//ListView 설정
		mArray = new ArrayList<ListItem>();
		listView = (ListView)rootView.findViewById(R.id.restaurant_listview);

		//Main, MyPage 구분해서 데이터 받아오기
		if(isMain) {
			//new AsyncTaskParseJson().execute();
		}
		else {
			addItemMyPage();
		}

		//List 생성
		setList(isMain);
	}

	//Main 데이터 추가
	private void addItemMain() {
		mArray = new ArrayList<ListItem>();
		mArray.add(new ListItem ("0", "meal", "정성효", "2014/12/27(토)", "인천","계양구","70,000", "15", "한식", "2014/12/30(화) 14:00","어머니가 차려주신 밥상같은 식당", "10"));
		mArray.add(new ListItem ("1", "meal", "정성효", "2014/12/29(월)", "서울","마포구","100,000", "12", "중식", "2014/12/30(화) 18:00","조용한 식당", "5"));
	}

	//MyPage 데이터 추가
	private void addItemMyPage() {
		mArray = new ArrayList<ListItem>();
		mArray.add(new ListItem ("2", "meal", "신소영", "2014/12/27(토)", "인천","계양구","70,000", "15", "한식", "2014/12/30(화) 14:00","어머니가 차려주신 밥상같은 식당", "10"));
		mArray.add(new ListItem ("3", "meal", "신소영", "2014/12/29(월)", "서울","마포구","100,000", "12", "중식", "2014/12/30(화) 18:00","조용한 식당", "5"));
	}

	//List 생성
	private void setList(boolean isMain) {
		adapter = new ListAdapter(mContext, mArray);
		adapter.notifyDataSetChanged();
		listView.setAdapter(adapter);	

		//마이페이지일때만 Listener 연결
		if(!isMain) {
			listView.setOnItemClickListener(itemListener);
			listView.setOnItemLongClickListener(itemLongListener);
		}
	}

	//ClickListener
	OnItemClickListener itemListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

			////아이템 아이디, 보드타입 넘겨주기 위해 저장 - 토스트에 띄우기
			selectedItemId = adapter.deleteItemId;
			selectedItemBoardType = adapter.deleteItemBoardType;
			Toast.makeText(mContext, selectedItemId+"+"+selectedItemBoardType, Toast.LENGTH_SHORT).show();
		}
	};

	//LongClickListener
	OnItemLongClickListener itemLongListener = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {

			//아이템 아이디, 보드타입 넘겨주기 위해 저장
			selectedItemId = adapter.deleteItemId;
			selectedItemBoardType = adapter.deleteItemBoardType;

			//AlertDialog 생성
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			builder.setMessage("해당 내역을 삭제하시겠습니까?");
			builder.setCancelable(true);

			//PositiveButton
			builder.setPositiveButton("네", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					//삭제하는 부분추가
					
					Toast.makeText(mContext, "삭제되었습니다.", Toast.LENGTH_LONG).show();
					new Thread(new Runnable() {
						public void run() {
							deleteBoard();						
						}
					}).start();
				}
			});

			//NegativeButton
			builder.setNegativeButton("아니오", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			});

			//Create and show the dialog
			builder.show();
			//Signal OK to avoid further processing of the long click
			return true;
		}
	};
	
	void deleteBoard () {
		Log.d("mypage", "hey");
		try {
			// http://s940217.mireene.com/get_register_hotel.php?location="전체"&type="전체"
			String surl = "http://s940217.mireene.com/delete_board.php?boardid=";
			surl = surl
					+ java.net.URLEncoder.encode(new String(selectedItemId
							.getBytes("UTF-8")));
			
			surl = surl + "&type=";
			surl = surl
					+ java.net.URLEncoder.encode(new String(selectedItemBoardType
							.getBytes("UTF-8")));
			
//			surl = surl + "&type=";
//			surl = surl+ selectedItemBoardType;
			System.out.println("mypage" + surl);
			Log.d("mypage", surl);
			URL url = new URL(surl);
			// URL url = new
			// URL("http://mapnoti.zz.mu/getdata.php?lat=37.5745215&lng=127.0388250&range=1.5");
			HttpURLConnection conn = (HttpURLConnection) url
					.openConnection();
			if (conn != null) {

				conn.setConnectTimeout(5000);
				// conn.setUseCaches(false);
				System.out.println("connect");
				System.out.println(String.valueOf(conn.getResponseCode())
						+ "\n" + String.valueOf(HttpURLConnection.HTTP_OK));

				if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
					BufferedReader br = new BufferedReader(
							new InputStreamReader(conn.getInputStream()));

//					while (true) {
//						String line = br.readLine();
//						if (line == null)
//							break;
//						sb.append(line + "\n");
//					}
					br.close();

				} else {

				}
				conn.disconnect();
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@Override
	public void onRefresh() {
		if (!mArray.isEmpty())
			mArray.clear();
		//Toast.makeText(mContext, restaurantSpinnerLocation.getSelectedItem().toString() + " / " + restaurantSpinnerKind.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
		new AsyncTaskParseJson().execute();
	}

	public class AsyncTaskParseJson extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			refreshLayout.setRefreshing(true);
			/*
			 * pDialog = new ProgressDialog(mContext);
			 * pDialog.setMessage("데이터 가져오는 중...");
			 * //pDialog.setIndeterminate(false); pDialog.setCancelable(true);
			 * pDialog.show();
			 */
		}

		@Override
		protected String doInBackground(String... arg0) {

			StringBuilder sb = new StringBuilder();

			try {
				SharedPreferences pref = mContext.getSharedPreferences("pref",
						mContext.MODE_PRIVATE);
				String username = pref.getString("username", "");
				if(isMain)
					username = "0";
				Log.d("meal", "username: "+ username);

				// http://s940217.mireene.com/get_register_hotel.php?location="전체"&type="전체"
				String surl = "http://s940217.mireene.com/get_meal.php?location=";
				surl = surl
						+ java.net.URLEncoder.encode(new String(restaurantSpinnerLocation.getSelectedItem().toString()
								.getBytes("UTF-8")));
				surl = surl + "&type=";
				surl = surl
						+ java.net.URLEncoder.encode(new String(restaurantSpinnerKind.getSelectedItem().toString()
								.getBytes("UTF-8")));
				surl = surl + "&username=";
				surl = surl
						+ java.net.URLEncoder.encode(username);

				Log.d("meal", surl);
				URL url = new URL(surl);
				// URL url = new
				// URL("http://mapnoti.zz.mu/getdata.php?lat=37.5745215&lng=127.0388250&range=1.5");
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				if (conn != null) {

					conn.setConnectTimeout(5000);
					// conn.setUseCaches(false);
					System.out.println("connect");
					System.out.println(String.valueOf(conn.getResponseCode())
							+ "\n" + String.valueOf(HttpURLConnection.HTTP_OK));

					if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
						BufferedReader br = new BufferedReader(
								new InputStreamReader(conn.getInputStream()));

						while (true) {
							String line = br.readLine();
							if (line == null)
								break;
							sb.append(line + "\n");
						}
						br.close();

					} else {

					}
					conn.disconnect();
				}
			} catch (Exception e) {
				// Log.d("hotel", "connection error: "+e.toString());
				Toast.makeText(mContext, e.toString(), Toast.LENGTH_SHORT)
				.show();
			}

			String jsonString = sb.toString();
			Log.d("meal", "jsonString " + jsonString);
			if(jsonString.length() > 5) {
				try {
					if (!mArray.isEmpty())
						mArray.clear();

					JSONArray ja = new JSONArray(jsonString);
					Log.d("meal", "ja.length: " + ja.length());

					for (int i = 0; i < ja.length(); i++) {
						JSONObject jo = ja.getJSONObject(i);

						String numberofpeople = jo.getString("numberofpeople");
						String now_date = jo.getString("now_date");// @drawable/img_nature1
						String schedule_to = jo.getString("schedule_to");
						String schedule_from = jo.getString("schedule_from");
						String departure = jo.getString("location_main");
						String destination = jo.getString("location_sub");
						String hotel_type = jo.getString("type");
						String hotel_budget = jo.getString("budget");
						String hotel_content = jo.getString("content");
						String user_id = jo.getString("user_id");
						String numberofbid = jo.getString("numberofbid");
						String id = jo.getString("id");

						mArray.add(new ListItem(id, "meal", user_id, now_date, departure,
								destination, hotel_budget, numberofpeople,
								hotel_type, schedule_from+"\n"+schedule_to, hotel_content, numberofbid));

					}
					// Log.d("hotel", ""+mArray.size());

				} catch (JSONException e) {
					Toast.makeText(mContext, e.toString(), Toast.LENGTH_SHORT)
					.show();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(String strFromDoInBg) {
			// pDialog.dismiss();
			refreshLayout.setRefreshing(false); // Refresh 애니메이션 Stop
			Log.d("meal", "onPostExecute");
			Log.d("meal", "" + mArray.size());
			adapter.notifyDataSetChanged();
		}

	}
}