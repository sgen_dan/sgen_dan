package com.example.danpoong.tab;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.example.danpoong.R;
import com.example.danpoong.list.ListAdapter;
import com.example.danpoong.list.ListItem;

public class HotelList implements SwipeRefreshLayout.OnRefreshListener {

	//Fragment
	private Context mContext;
	private View mRootView;
	private boolean isMain;

	//List
	private ArrayList<ListItem> mArray;
	private ListAdapter adapter;
	ListView listView;
	// private ProgressDialog pDialog;
	private SwipeRefreshLayout refreshLayout;

	String hotel_location = "전체";
	String hotel_type = "전체";

	public HotelList(Context mContext, View rootView, boolean isMain) {
		
		//SharedPreferences
		SharedPreferences pref = mContext.getSharedPreferences("pref", mContext.MODE_PRIVATE);
		int is_store = pref.getInt("is_store", 0);

		this.mContext = mContext;
		this.mRootView = rootView;
		this.isMain = isMain;
		mArray = new ArrayList<ListItem>();
		setList();
		// Refresh 설정
		refreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_hotel);
		refreshLayout.setColorScheme(android.R.color.holo_blue_bright,android.R.color.holo_green_light,android.R.color.holo_orange_light,android.R.color.holo_red_light);
		refreshLayout.setOnRefreshListener(this);
		refreshLayout.setRefreshing(true);

		hotel_location = "전체";
		hotel_type = "전체";

		new AsyncTaskParseJson().execute();
	}

	public HotelList(Context mContext, View rootView, String location, String type, boolean isMain) {

		this.mContext = mContext;
		this.mRootView = rootView;
		this.isMain = isMain;

		mArray = new ArrayList<ListItem>();

		setList();

		// Refresh 설정
		refreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_hotel);
		refreshLayout.setColorScheme(android.R.color.holo_blue_bright,android.R.color.holo_green_light,android.R.color.holo_orange_light,android.R.color.holo_red_light);
		refreshLayout.setOnRefreshListener(this);
		refreshLayout.setRefreshing(true);

		hotel_location = location;
		hotel_type = type;

		//Toast.makeText(mContext, hotel_location + " / " + hotel_type,Toast.LENGTH_SHORT).show();

		new AsyncTaskParseJson().execute();
	}

	// List 생성
	private void setList() {
		listView = (ListView) mRootView.findViewById(R.id.hotel_listview);
		adapter = new ListAdapter(mContext, mArray);
		adapter.notifyDataSetChanged();
		listView.setAdapter(adapter);
	}

	@Override
	public void onRefresh() {
		Toast.makeText(mContext, hotel_location + " / " + hotel_type,Toast.LENGTH_SHORT).show();
		new AsyncTaskParseJson().execute();
	}

	public class AsyncTaskParseJson extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			refreshLayout.setRefreshing(true);
			/*
			 * pDialog = new ProgressDialog(mContext);
			 * pDialog.setMessage("데이터 가져오는 중...");
			 * //pDialog.setIndeterminate(false); pDialog.setCancelable(true);
			 * pDialog.show();
			 */
		}

		@Override
		protected String doInBackground(String... arg0) {

			StringBuilder sb = new StringBuilder();

			try {

				// http://s940217.mireene.com/get_register_hotel.php?location="전체"&type="전체"
				String surl = "http://s940217.mireene.com/get_hotel.php?location=";
				surl = surl
						+ java.net.URLEncoder.encode(new String(hotel_location
								.getBytes("UTF-8")));
				surl = surl + "&type=";
				surl = surl
						+ java.net.URLEncoder.encode(new String(hotel_type
								.getBytes("UTF-8")));

				Log.d("hotel", surl);
				URL url = new URL(surl);
				// URL url = new
				// URL("http://mapnoti.zz.mu/getdata.php?lat=37.5745215&lng=127.0388250&range=1.5");
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				if (conn != null) {

					conn.setConnectTimeout(5000);
					// conn.setUseCaches(false);
					System.out.println("connect");
					System.out.println(String.valueOf(conn.getResponseCode())
							+ "\n" + String.valueOf(HttpURLConnection.HTTP_OK));

					if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
						BufferedReader br = new BufferedReader(
								new InputStreamReader(conn.getInputStream()));

						while (true) {
							String line = br.readLine();
							if (line == null)
								break;
							sb.append(line + "\n");
						}
						br.close();

					} else {

					}
					conn.disconnect();
				}
			} catch (Exception e) {
				// Log.d("hotel", "connection error: "+e.toString());
				Toast.makeText(mContext, e.toString(), Toast.LENGTH_SHORT)
				.show();
			}

			String jsonString = sb.toString();
			Log.d("hotel", "jsonString " + jsonString);
			if(jsonString.length() > 5) {
				try {
					if (!mArray.isEmpty())
						mArray.clear();

					JSONArray ja = new JSONArray(jsonString);
					Log.d("hotel", "ja.length: " + ja.length());

					for (int i = 0; i < ja.length(); i++) {
						JSONObject jo = ja.getJSONObject(i);

						String numberofpeople = jo.getString("numberofpeople");
						String now_date = jo.getString("now_date");// @drawable/img_nature1
						String schedule_to = jo.getString("schedule_to");
						String schedule_from = jo.getString("schedule_from");
						String departure = jo.getString("location_main");
						String destination = jo.getString("location_sub");
						String hotel_type = jo.getString("type");
						String hotel_budget = jo.getString("budget");
						String hotel_content = jo.getString("content");
						String user_id = jo.getString("user_id");
						String numberofbid = jo.getString("numberofbid");
						String id = jo.getString("id");

						mArray.add(new ListItem(id, "hotel", user_id, now_date, departure,
								destination, hotel_budget, numberofpeople,
								hotel_type, schedule_from+"\n"+schedule_to, hotel_content, numberofbid));

					}
					// Log.d("hotel", ""+mArray.size());

				} catch (JSONException e) {
					// Log.d("hotel", "Json Array Error: "+e.toString());
					Toast.makeText(mContext, e.toString(), Toast.LENGTH_SHORT)
					.show();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(String strFromDoInBg) {
			// pDialog.dismiss();
			refreshLayout.setRefreshing(false); // Refresh 애니메이션 Stop
			Log.d("hotel", "onPostExecute");
			Log.d("hotel", "" + mArray.size());
			adapter.notifyDataSetChanged();
		}

	}
}
